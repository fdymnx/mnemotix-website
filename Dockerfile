FROM php:7.2-apache
RUN apt-get update && apt-get install -y msmtp
ADD config/mail.ini /usr/local/etc/php/conf.d/
COPY config/msmtp.conf /etc/msmtprc
COPY www /var/www/html/
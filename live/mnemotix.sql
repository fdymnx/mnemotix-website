SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `mnemotix` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mnemotix`;

DROP TABLE IF EXISTS `mnx_commentmeta`;
CREATE TABLE IF NOT EXISTS `mnx_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mnx_comments`;
CREATE TABLE IF NOT EXISTS `mnx_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mnx_links`;
CREATE TABLE IF NOT EXISTS `mnx_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `mnx_options`;
CREATE TABLE IF NOT EXISTS `mnx_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM AUTO_INCREMENT=10744 DEFAULT CHARSET=utf8;

INSERT INTO `mnx_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://mnemotix.com/live', 'yes'),
(2, 'home', 'http://mnemotix.com/live', 'yes'),
(3, 'blogname', 'Mnemotix', 'yes'),
(4, 'blogdescription', 'du sens à vos données', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'pymichel@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '4', 'yes'),
(13, 'rss_use_excerpt', '1', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'closed', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'G \\h i \\m\\i\\n', 'yes'),
(25, 'links_updated_date_format', 'j F Y G \\h i \\m\\i\\n', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%category%/%postname%/', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:1;s:25:"bwp-minify/bwp-minify.php";i:2;s:28:"category-posts/cat-posts.php";i:3;s:53:"codestyling-localization/codestyling-localization.php";i:5;s:55:"display-categories-widget/display_categories_widget.php";i:7;s:29:"widget-logic/widget_logic.php";i:10;s:42:"yet-another-related-posts-plugin/yarpp.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/\nhttp://ping.feedburner.com/\nhttp://blogsearch.google.co.uk/ping/RPC2\nhttp://blogsearch.google.com/ping/RPC2\nhttp://blogsearch.google.be/ping/RPC2\nhttp://blogsearch.google.de/ping/RPC2\nhttp://blogsearch.google.es/ping/RPC2\nhttp://blogsearch.google.fr/ping/RPC2\nhttp://blogsearch.google.us/ping/RPC2\nhttp://blog.goo.ne.jp/XMLRPC\nhttp://blog.with2.net/ping.php\nhttp://www.feedsky.com/api/RPC2\nhttp://ping.fc2.com/\nhttp://ping.syndic8.com/xmlrpc.php\nhttp://rpc.blogrolling.com/pinger/\nhttp://rpc.weblogs.com/RPC2\nhttp://serenebach.net/rep.cgi\nhttp://services.newsgator.com/ngws/xmlrpcping.aspx\nhttp://www.wasalive.com/ping/\nhttp://www.xianguo.com/xmlrpc/ping.php\nhttp://ping.bitacoras.com/\nhttp://ping.blo.gs/\nhttp://tweetmeme.com/update/ping\nhttp://www.bing.com/webmaster/ping.aspx?siteMap=\nhttp://www.i-learn.jp/ping/\nhttp://www.bloggybuzz.com/ping.php\nhttp://rpc.twingly.com/\nhttp://xping.pubsub.com/ping/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '1', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', 'a:5:{i:0;s:55:"/home/mnemotix/www/live/wp-content/themes/mnx/style.css";i:2;s:56:"/home/mnemotix/www/live/wp-content/themes/mnx/footer.php";i:3;s:56:"/home/mnemotix/www/live/wp-content/themes/mnx/header.php";i:4;s:59:"/home/mnemotix/www/live/wp-content/themes/mnx/functions.php";i:5;s:57:"/home/mnemotix/www/live/wp-content/themes/mnx/archive.php";}', 'no'),
(41, 'template', 'mnx', 'yes'),
(42, 'stylesheet', 'mnx', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '31536', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '1', 'yes'),
(53, 'default_link_category', '0', 'yes'),
(54, 'show_on_front', 'posts', 'yes'),
(55, 'tag_base', '/themes', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '594', 'yes'),
(63, 'medium_size_h', '594', 'yes'),
(64, 'avatar_default', 'mystery', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', '', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(82, 'uninstall_plugins', 'a:2:{s:29:"easy-wp-smtp/easy-wp-smtp.php";s:22:"swpsmtp_send_uninstall";s:27:"wp-super-cache/wp-cache.php";s:23:"wpsupercache_deactivate";}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '0', 'yes'),
(85, 'page_on_front', '0', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '29630', 'yes'),
(89, 'mnx_user_roles', 'a:8:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:14:"backwpup_admin";a:2:{s:4:"name";s:14:"BackWPup Admin";s:12:"capabilities";a:10:{s:8:"backwpup";b:1;s:13:"backwpup_jobs";b:1;s:18:"backwpup_jobs_edit";b:1;s:19:"backwpup_jobs_start";b:1;s:16:"backwpup_backups";b:1;s:25:"backwpup_backups_download";b:1;s:23:"backwpup_backups_delete";b:1;s:13:"backwpup_logs";b:1;s:20:"backwpup_logs_delete";b:1;s:17:"backwpup_settings";b:1;}}s:14:"backwpup_check";a:2:{s:4:"name";s:21:"BackWPup jobs checker";s:12:"capabilities";a:10:{s:8:"backwpup";b:1;s:13:"backwpup_jobs";b:1;s:18:"backwpup_jobs_edit";b:0;s:19:"backwpup_jobs_start";b:0;s:16:"backwpup_backups";b:1;s:25:"backwpup_backups_download";b:0;s:23:"backwpup_backups_delete";b:0;s:13:"backwpup_logs";b:1;s:20:"backwpup_logs_delete";b:0;s:17:"backwpup_settings";b:0;}}s:15:"backwpup_helper";a:2:{s:4:"name";s:20:"BackWPup jobs helper";s:12:"capabilities";a:10:{s:8:"backwpup";b:1;s:13:"backwpup_jobs";b:1;s:18:"backwpup_jobs_edit";b:0;s:19:"backwpup_jobs_start";b:1;s:16:"backwpup_backups";b:1;s:25:"backwpup_backups_download";b:1;s:23:"backwpup_backups_delete";b:1;s:13:"backwpup_logs";b:1;s:20:"backwpup_logs_delete";b:1;s:17:"backwpup_settings";b:0;}}}', 'yes'),
(90, 'WPLANG', 'fr_FR', 'yes'),
(91, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-posts', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_recent-comments', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_archives', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_meta', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'sidebars_widgets', 'a:6:{s:19:"wp_inactive_widgets";a:2:{i:0;s:11:"tag_cloud-2";i:1;s:8:"search-2";}s:8:"footer-a";a:0:{}s:8:"footer-b";a:0:{}s:8:"footer-c";a:0:{}s:7:"sidebar";a:6:{i:0;s:24:"hemingway_video_widget-2";i:1;s:25:"displaycategorieswidget-2";i:2;s:16:"category-posts-3";i:3;s:16:"category-posts-2";i:4;s:16:"category-posts-4";i:5;s:16:"category-posts-5";}s:13:"array_version";i:3;}', 'yes'),
(97, 'cron', 'a:4:{i:1454377630;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1454420848;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1454425861;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(101, '_transient_random_seed', '344d3cffb2e7f3e19dec4e264bf08edc', 'yes'),
(132, 'theme_mods_twentyfourteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1413813169;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(10741, '_site_transient_timeout_theme_roots', '1454360640', 'yes'),
(10742, '_site_transient_theme_roots', 'a:1:{s:3:"mnx";s:7:"/themes";}', 'yes'),
(10743, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1454358842;s:8:"response";a:8:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"3.1.7";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.3.1.7.zip";}s:21:"backwpup/backwpup.php";O:8:"stdClass":6:{s:2:"id";s:4:"8736";s:4:"slug";s:8:"backwpup";s:6:"plugin";s:21:"backwpup/backwpup.php";s:11:"new_version";s:5:"3.2.3";s:3:"url";s:39:"https://wordpress.org/plugins/backwpup/";s:7:"package";s:57:"https://downloads.wordpress.org/plugin/backwpup.3.2.3.zip";}s:28:"category-posts/cat-posts.php";O:8:"stdClass":6:{s:2:"id";s:4:"1199";s:4:"slug";s:14:"category-posts";s:6:"plugin";s:28:"category-posts/cat-posts.php";s:11:"new_version";s:5:"4.1.4";s:3:"url";s:45:"https://wordpress.org/plugins/category-posts/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/category-posts.4.1.4.zip";}s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":6:{s:2:"id";s:3:"790";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:5:"4.3.1";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/contact-form-7.4.3.1.zip";}s:27:"wp-super-cache/wp-cache.php";O:8:"stdClass":7:{s:2:"id";s:4:"1221";s:4:"slug";s:14:"wp-super-cache";s:6:"plugin";s:27:"wp-super-cache/wp-cache.php";s:11:"new_version";s:5:"1.4.7";s:3:"url";s:45:"https://wordpress.org/plugins/wp-super-cache/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/wp-super-cache.1.4.7.zip";s:14:"upgrade_notice";s:61:"Bugfix for layout changes on settings pages in WordPress 4.4.";}s:42:"yet-another-related-posts-plugin/yarpp.php";O:8:"stdClass":6:{s:2:"id";s:4:"1418";s:4:"slug";s:32:"yet-another-related-posts-plugin";s:6:"plugin";s:42:"yet-another-related-posts-plugin/yarpp.php";s:11:"new_version";s:5:"4.3.1";s:3:"url";s:63:"https://wordpress.org/plugins/yet-another-related-posts-plugin/";s:7:"package";s:81:"https://downloads.wordpress.org/plugin/yet-another-related-posts-plugin.4.3.1.zip";}s:24:"wordpress-seo/wp-seo.php";O:8:"stdClass":6:{s:2:"id";s:4:"5899";s:4:"slug";s:13:"wordpress-seo";s:6:"plugin";s:24:"wordpress-seo/wp-seo.php";s:11:"new_version";s:5:"3.0.7";s:3:"url";s:44:"https://wordpress.org/plugins/wordpress-seo/";s:7:"package";s:62:"https://downloads.wordpress.org/plugin/wordpress-seo.3.0.7.zip";}s:51:"youtube-channel-gallery/youtube-channel-gallery.php";O:8:"stdClass":6:{s:2:"id";s:5:"32977";s:4:"slug";s:23:"youtube-channel-gallery";s:6:"plugin";s:51:"youtube-channel-gallery/youtube-channel-gallery.php";s:11:"new_version";s:3:"2.4";s:3:"url";s:54:"https://wordpress.org/plugins/youtube-channel-gallery/";s:7:"package";s:70:"https://downloads.wordpress.org/plugin/youtube-channel-gallery.2.4.zip";}}s:12:"translations";a:0:{}s:9:"no_update";a:4:{s:25:"bwp-minify/bwp-minify.php";O:8:"stdClass":6:{s:2:"id";s:5:"21792";s:4:"slug";s:10:"bwp-minify";s:6:"plugin";s:25:"bwp-minify/bwp-minify.php";s:11:"new_version";s:5:"1.3.3";s:3:"url";s:41:"https://wordpress.org/plugins/bwp-minify/";s:7:"package";s:59:"https://downloads.wordpress.org/plugin/bwp-minify.1.3.3.zip";}s:55:"display-categories-widget/display_categories_widget.php";O:8:"stdClass":6:{s:2:"id";s:5:"33915";s:4:"slug";s:25:"display-categories-widget";s:6:"plugin";s:55:"display-categories-widget/display_categories_widget.php";s:11:"new_version";s:3:"1.1";s:3:"url";s:56:"https://wordpress.org/plugins/display-categories-widget/";s:7:"package";s:68:"https://downloads.wordpress.org/plugin/display-categories-widget.zip";}s:29:"easy-wp-smtp/easy-wp-smtp.php";O:8:"stdClass":6:{s:2:"id";s:5:"40147";s:4:"slug";s:12:"easy-wp-smtp";s:6:"plugin";s:29:"easy-wp-smtp/easy-wp-smtp.php";s:11:"new_version";s:5:"1.2.2";s:3:"url";s:43:"https://wordpress.org/plugins/easy-wp-smtp/";s:7:"package";s:55:"https://downloads.wordpress.org/plugin/easy-wp-smtp.zip";}s:29:"widget-logic/widget_logic.php";O:8:"stdClass":6:{s:2:"id";s:4:"2545";s:4:"slug";s:12:"widget-logic";s:6:"plugin";s:29:"widget-logic/widget_logic.php";s:11:"new_version";s:4:"0.57";s:3:"url";s:43:"https://wordpress.org/plugins/widget-logic/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/widget-logic.0.57.zip";}}}', 'yes'),
(7938, '_transient_timeout_feed_d4957ea5e040d114212525f016687ba9', '1439157146', 'no'),
(7939, '_transient_feed_d4957ea5e040d114212525f016687ba9', 'a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"\n\n\n";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:31:"\n	\n	\n	\n	\n	\n	\n	\n	\n	\n	\n		\n		\n		\n	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"MarketPress » BackWPup";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:23:"https://marketpress.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:86:"Add rocket science to your website with our professional WordPress Plugins and Themes.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sun, 09 Aug 2015 02:58:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:29:"http://wordpress.org/?v=4.1.5";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:4:{i:0;a:6:{s:4:"data";s:39:"\n		\n		\n		\n		\n		\n				\n		\n\n		\n		\n		\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:50:"BackWPup Pro: Saving a WordPress backup to Dropbox";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:54:"https://marketpress.com/2014/wordpress-backup-dropbox/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:63:"https://marketpress.com/2014/wordpress-backup-dropbox/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 25 Nov 2014 18:31:51 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:8:"BackWPup";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:6:"Plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://marketpress.com/?p=35552";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:328:"Saving WordPress backups to a Dropbox may not always be easy, as it requires a one-time authentication. We will show you how the connection works using our plugins BackWPup Pro or BackWPup. The procedure described here remains the same if you want to do a one-time backup or want to schedule periodic backups (e.g. via [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"enclosure";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"url";s:79:"https://marketpress.com/wp-content/uploads/2014/11/backwpup-dropbox-150x150.png";s:6:"length";s:5:"11723";s:4:"type";s:9:"image/png";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Michael Firnkes";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:59:"https://marketpress.com/2014/wordpress-backup-dropbox/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"4";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:36:"\n		\n		\n		\n		\n		\n				\n\n		\n		\n		\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:47:"World Backup Day – win a BackWPup Pro license";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:71:"https://marketpress.com/2014/world-backup-day-win-backwpup-pro-license/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:80:"https://marketpress.com/2014/world-backup-day-win-backwpup-pro-license/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 31 Mar 2014 10:15:16 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:8:"BackWPup";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://marketpress.com/?p=27516";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:310:"Who ever runs a website with WordPress should have a routine for regular backups going on in order to avoid data loss. Today is World Backup Day. On this occasion we are giving away three licenses of BackWPup Pro. The real significance of a backup only comes in effect when your data is already gone. [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"enclosure";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"url";s:76:"https://marketpress.com/wp-content/uploads/2014/03/201403wbdlogo-150x150.jpg";s:6:"length";s:4:"9311";s:4:"type";s:10:"image/jpeg";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"Olaf Schmitz";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:76:"https://marketpress.com/2014/world-backup-day-win-backwpup-pro-license/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:2:"19";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:63:"\n		\n		\n		\n		\n		\n				\n		\n		\n		\n		\n		\n		\n		\n		\n		\n\n		\n		\n		\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:77:"Backup your WordPress – Using BackWPup Archives with AWS Glacier (Tutorial)";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:81:"https://marketpress.com/2014/wordpress-backup-backwpup-archives-with-aws-glacier/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:90:"https://marketpress.com/2014/wordpress-backup-backwpup-archives-with-aws-glacier/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 21 Jan 2014 16:05:54 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:10:{i:0;a:5:{s:4:"data";s:8:"BackWPup";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:7:"Plugins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:8:"Tutorial";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:6:"amazon";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:3:"aws";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:6:"backup";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:7:"glacier";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:7;a:5:{s:4:"data";s:6:"Plugin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:8;a:5:{s:4:"data";s:8:"tutorial";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:9;a:5:{s:4:"data";s:9:"wordpress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://marketpress.com/?p=20232";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:333:"BackWPup includes a S3 connection which we can use with Amazon&#8217;s Service Glacier. Amazon Glacier was developed to store or archive data that we don&#8217;t use regularly for a long time, thus significantly reducing the costs for storing the archives. However, it can take up to 9 hours to access the data. 4 hours for [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:14:"Daniel Hüsken";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:86:"https://marketpress.com/2014/wordpress-backup-backwpup-archives-with-aws-glacier/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:45:"\n		\n		\n		\n		\n		\n				\n		\n		\n		\n\n		\n		\n		\n		\n		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:108:"Adventskalender Day 22 – We celebrate 1 Mio downloads of BackWPup with a new release and 33% off till NYE!";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"https://marketpress.com/2013/celebrate-1-mio-downloads-of-backwpup-33-off-till-nye/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:92:"https://marketpress.com/2013/celebrate-1-mio-downloads-of-backwpup-33-off-till-nye/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 21 Dec 2013 23:00:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:4:{i:0;a:5:{s:4:"data";s:15:"Adventskalender";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:8:"BackWPup";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:7:"Plugins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:9:"wordpress";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:32:"https://marketpress.com/?p=19552";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:349:"Today we gonna party! Why? &#8220;Just&#8221; because it is the 4th Advent, Christmas is coming and again one of our products will get a special price? No! Today there is another great cause: Our worldwide popular WordPress backup plugin BackWPup will reach download number 1 million &#8211; exactly today! Therefor we released a brand new [&#8230;]";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"enclosure";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"url";s:99:"https://marketpress.com/wp-content/uploads/2013/12/201312marketpress-adventskalender-22-150x150.jpg";s:6:"length";s:4:"6366";s:4:"type";s:10:"image/jpeg";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:15:"Manuel Schmutte";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:88:"https://marketpress.com/2013/celebrate-1-mio-downloads-of-backwpup-33-off-till-nye/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"href";s:51:"https://marketpress.com/news/plugins/backwpup/feed/";s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:44:"http://purl.org/rss/1.0/modules/syndication/";a:2:{s:12:"updatePeriod";a:1:{i:0;a:5:{s:4:"data";s:6:"hourly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"updateFrequency";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:8:{s:4:"date";s:29:"Sun, 09 Aug 2015 09:52:25 GMT";s:6:"server";s:6:"Apache";s:12:"x-powered-by";s:9:"PHP/5.3.8";s:10:"x-pingback";s:34:"https://marketpress.com/xmlrpc.php";s:13:"last-modified";s:29:"Sun, 09 Aug 2015 02:58:11 GMT";s:4:"vary";s:26:"User-Agent,Accept-Encoding";s:10:"connection";s:5:"close";s:12:"content-type";s:23:"text/xml; charset=UTF-8";}s:5:"build";s:14:"20141020134043";}', 'no'),
(7940, '_transient_timeout_feed_mod_d4957ea5e040d114212525f016687ba9', '1439157146', 'no'),
(7941, '_transient_feed_mod_d4957ea5e040d114212525f016687ba9', '1439113946', 'no'),
(7943, '_transient_timeout_plugin_slugs', '1439200570', 'no'),
(7944, '_transient_plugin_slugs', 'a:13:{i:0;s:19:"akismet/akismet.php";i:1;s:21:"backwpup/backwpup.php";i:2;s:25:"bwp-minify/bwp-minify.php";i:3;s:28:"category-posts/cat-posts.php";i:4;s:53:"codestyling-localization/codestyling-localization.php";i:5;s:36:"contact-form-7/wp-contact-form-7.php";i:6;s:55:"display-categories-widget/display_categories_widget.php";i:7;s:29:"easy-wp-smtp/easy-wp-smtp.php";i:8;s:29:"widget-logic/widget_logic.php";i:9;s:27:"wp-super-cache/wp-cache.php";i:10;s:42:"yet-another-related-posts-plugin/yarpp.php";i:11;s:24:"wordpress-seo/wp-seo.php";i:12;s:51:"youtube-channel-gallery/youtube-channel-gallery.php";}', 'no'),
(7423, '_site_transient_timeout_browser_e14f41f376cc3701f03d93ea21192f2a', '1438094227', 'yes'),
(7424, '_site_transient_browser_e14f41f376cc3701f03d93ea21192f2a', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:7:"Firefox";s:7:"version";s:4:"39.0";s:10:"update_url";s:23:"http://www.firefox.com/";s:7:"img_src";s:50:"http://s.wordpress.org/images/browsers/firefox.png";s:11:"img_src_ssl";s:49:"https://wordpress.org/images/browsers/firefox.png";s:15:"current_version";s:2:"16";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(7445, 'widget_yarpp_widget', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(522, 'wpseo', 'a:24:{s:14:"blocking_files";a:0:{}s:26:"ignore_blog_public_warning";b:0;s:31:"ignore_meta_description_warning";b:0;s:20:"ignore_page_comments";b:0;s:16:"ignore_permalink";b:0;s:11:"ignore_tour";b:1;s:15:"ms_defaults_set";b:0;s:23:"theme_description_found";s:0:"";s:21:"theme_has_description";b:0;s:19:"tracking_popup_done";b:1;s:7:"version";s:5:"2.1.1";s:10:"seen_about";b:1;s:11:"alexaverify";s:0:"";s:12:"company_logo";s:0:"";s:12:"company_name";s:0:"";s:17:"company_or_person";s:0:"";s:20:"disableadvanced_meta";b:1;s:12:"googleverify";s:0:"";s:8:"msverify";s:0:"";s:11:"person_name";s:0:"";s:12:"website_name";s:0:"";s:22:"alternate_website_name";s:0:"";s:12:"yandexverify";s:0:"";s:14:"yoast_tracking";b:0;}', 'yes'),
(578, 'ossdl_cname', '', 'yes'),
(564, 'wpseo_taxonomy_meta', 'a:0:{}', 'yes'),
(528, 'wpseo_xml', 'a:17:{s:22:"disable_author_sitemap";b:1;s:22:"disable_author_noposts";b:1;s:16:"enablexmlsitemap";b:1;s:16:"entries-per-page";i:1000;s:14:"xml_ping_yahoo";b:1;s:12:"xml_ping_ask";b:1;s:38:"user_role-administrator-not_in_sitemap";b:0;s:31:"user_role-editor-not_in_sitemap";b:0;s:31:"user_role-author-not_in_sitemap";b:0;s:36:"user_role-contributor-not_in_sitemap";b:0;s:35:"user_role-subscriber-not_in_sitemap";b:0;s:30:"post_types-post-not_in_sitemap";b:0;s:30:"post_types-page-not_in_sitemap";b:0;s:36:"post_types-attachment-not_in_sitemap";b:1;s:34:"taxonomies-category-not_in_sitemap";b:0;s:34:"taxonomies-post_tag-not_in_sitemap";b:0;s:37:"taxonomies-post_format-not_in_sitemap";b:0;}', 'yes'),
(480, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(486, 'widget_displaycategorieswidget', 'a:2:{i:2;a:8:{s:5:"title";s:19:"Catégories du blog";s:8:"category";s:1:"1";s:9:"dcw_limit";s:0:"";s:14:"display_parent";s:1:"0";s:24:"display_empty_categories";s:1:"1";s:15:"showcount_value";s:1:"1";s:11:"show_format";s:1:"0";s:10:"dcw_column";s:1:"1";}s:12:"_multiwidget";i:1;}', 'yes'),
(523, 'wpseo_permalinks', 'a:13:{s:15:"cleanpermalinks";b:0;s:24:"cleanpermalink-extravars";s:0:"";s:29:"cleanpermalink-googlecampaign";b:0;s:31:"cleanpermalink-googlesitesearch";b:0;s:15:"cleanreplytocom";b:0;s:10:"cleanslugs";b:1;s:14:"hide-feedlinks";b:0;s:12:"hide-rsdlink";b:0;s:14:"hide-shortlink";b:0;s:16:"hide-wlwmanifest";b:0;s:18:"redirectattachment";b:0;s:17:"stripcategorybase";b:1;s:13:"trailingslash";b:0;}', 'yes'),
(524, 'wpseo_titles', 'a:54:{s:10:"title_test";i:0;s:17:"forcerewritetitle";b:0;s:9:"separator";s:7:"sc-dash";s:5:"noodp";b:0;s:6:"noydir";b:0;s:15:"usemetakeywords";b:0;s:16:"title-home-wpseo";s:42:"%%sitename%% %%page%% %%sep%% %%sitedesc%%";s:18:"title-author-wpseo";s:36:"%%name%%, pour %%sitename%% %%page%%";s:19:"title-archive-wpseo";s:38:"%%date%% %%page%% %%sep%% %%sitename%%";s:18:"title-search-wpseo";s:65:"Vous avez cherché %%searchphrase%% %%page%% %%sep%% %%sitename%%";s:15:"title-404-wpseo";s:37:"Page introuvable %%sep%% %%sitename%%";s:19:"metadesc-home-wpseo";s:0:"";s:21:"metadesc-author-wpseo";s:0:"";s:22:"metadesc-archive-wpseo";s:0:"";s:18:"metakey-home-wpseo";s:0:"";s:20:"metakey-author-wpseo";s:0:"";s:22:"noindex-subpages-wpseo";b:0;s:20:"noindex-author-wpseo";b:0;s:21:"noindex-archive-wpseo";b:1;s:14:"disable-author";b:0;s:12:"disable-date";b:0;s:10:"title-post";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:13:"metadesc-post";s:0:"";s:12:"metakey-post";s:0:"";s:12:"noindex-post";b:0;s:13:"showdate-post";b:1;s:16:"hideeditbox-post";b:0;s:10:"title-page";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:13:"metadesc-page";s:0:"";s:12:"metakey-page";s:0:"";s:12:"noindex-page";b:0;s:13:"showdate-page";b:0;s:16:"hideeditbox-page";b:0;s:16:"title-attachment";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:19:"metadesc-attachment";s:0:"";s:18:"metakey-attachment";s:0:"";s:18:"noindex-attachment";b:0;s:19:"showdate-attachment";b:0;s:22:"hideeditbox-attachment";b:0;s:18:"title-tax-category";s:53:"%%term_title%% Archives %%page%% %%sep%% %%sitename%%";s:21:"metadesc-tax-category";s:0:"";s:20:"metakey-tax-category";s:0:"";s:24:"hideeditbox-tax-category";b:0;s:20:"noindex-tax-category";b:0;s:18:"title-tax-post_tag";s:53:"%%term_title%% Archives %%page%% %%sep%% %%sitename%%";s:21:"metadesc-tax-post_tag";s:0:"";s:20:"metakey-tax-post_tag";s:0:"";s:24:"hideeditbox-tax-post_tag";b:0;s:20:"noindex-tax-post_tag";b:0;s:21:"title-tax-post_format";s:53:"%%term_title%% Archives %%page%% %%sep%% %%sitename%%";s:24:"metadesc-tax-post_format";s:0:"";s:23:"metakey-tax-post_format";s:0:"";s:27:"hideeditbox-tax-post_format";b:0;s:23:"noindex-tax-post_format";b:1;}', 'yes'),
(525, 'wpseo_social', 'a:15:{s:9:"fb_admins";a:0:{}s:6:"fbapps";a:0:{}s:12:"fbconnectkey";s:32:"7c25f095acd7d33b22be259e780a3061";s:13:"facebook_site";s:0:"";s:16:"og_default_image";s:0:"";s:18:"og_frontpage_title";s:0:"";s:17:"og_frontpage_desc";s:0:"";s:18:"og_frontpage_image";s:0:"";s:9:"opengraph";b:1;s:10:"googleplus";b:0;s:14:"plus-publisher";s:0:"";s:7:"twitter";b:0;s:12:"twitter_site";s:0:"";s:17:"twitter_card_type";s:7:"summary";s:10:"fbadminapp";i:0;}', 'yes'),
(526, 'wpseo_rss', 'a:2:{s:9:"rssbefore";s:0:"";s:8:"rssafter";s:64:"Cet article %%POSTLINK%% est apparu en premier sur %%BLOGLINK%%.";}', 'yes'),
(527, 'wpseo_internallinks', 'a:10:{s:20:"breadcrumbs-404crumb";s:34:"Erreur 404&nbsp;: Page introuvable";s:23:"breadcrumbs-blog-remove";b:0;s:20:"breadcrumbs-boldlast";b:0;s:25:"breadcrumbs-archiveprefix";s:13:"Archives pour";s:18:"breadcrumbs-enable";b:0;s:16:"breadcrumbs-home";s:7:"Accueil";s:18:"breadcrumbs-prefix";s:0:"";s:24:"breadcrumbs-searchprefix";s:18:"Vous avez cherché";s:15:"breadcrumbs-sep";s:7:"&raquo;";s:23:"post_types-post-maintax";i:0;}', 'yes'),
(133, 'current_theme', 'MNX', 'yes'),
(134, 'theme_mods_mnx', 'a:9:{i:0;b:0;s:16:"background_color";s:0:"";s:16:"background_image";s:0:"";s:17:"background_repeat";s:6:"repeat";s:21:"background_position_x";s:4:"left";s:21:"background_attachment";s:6:"scroll";s:12:"accent_color";s:7:"#f27a24";s:14:"hemingway_logo";s:0:"";s:18:"nav_menu_locations";a:0:{}}', 'yes'),
(135, 'theme_switched', '', 'yes'),
(142, 'recently_activated', 'a:1:{s:21:"backwpup/backwpup.php";i:1439114169;}', 'yes'),
(7404, '_transient_timeout_feed_96281909e104f3c547a3bba0b6d36ad5', '1437524591', 'no'),
(7399, '_site_transient_timeout_browser_3a343c40138c409717df5b87960d9dd7', '1438086186', 'yes'),
(7400, '_site_transient_browser_3a343c40138c409717df5b87960d9dd7', 'a:9:{s:8:"platform";s:9:"Macintosh";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"43.0.2357.134";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(168, 'widget_hemingway_video_widget', 'a:3:{i:1;a:0:{}i:2;a:2:{s:12:"widget_title";s:0:"";s:9:"video_url";s:46:"http://vimeo.com/channels/staffpicks/106835400";}s:12:"_multiwidget";i:1;}', 'yes'),
(7932, '_site_transient_timeout_browser_a1ca93f38b1cae5c657b0f187b14c636', '1439718688', 'yes'),
(7933, '_site_transient_browser_a1ca93f38b1cae5c657b0f187b14c636', 'a:9:{s:8:"platform";s:7:"Windows";s:4:"name";s:6:"Chrome";s:7:"version";s:13:"44.0.2403.130";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(7934, 'can_compress_scripts', '0', 'yes'),
(166, 'widget_hemingway_dribbble_widget', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(167, 'widget_hemingway_flickr_widget', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(169, 'widget_pages', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(170, 'widget_calendar', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(171, 'widget_tag_cloud', 'a:3:{i:1;a:0:{}i:2;a:2:{s:5:"title";s:7:"Thèmes";s:8:"taxonomy";s:8:"post_tag";}s:12:"_multiwidget";i:1;}', 'yes'),
(172, 'widget_nav_menu', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(194, 'wpcf7', 'a:1:{s:7:"version";s:5:"4.1.2";}', 'yes'),
(299, 'widget_logic', 'a:8:{s:16:"category-posts-2";s:45:"is_category(5)||is_single() && in_category(5)";s:16:"category-posts-3";s:87:"is_category(array(1,3,10,11,12,13))||is_single() && in_category(array(1,3,10,11,12,13))";s:16:"category-posts-4";s:45:"is_category(4)||is_single() && in_category(4)";s:11:"tag_cloud-2";s:110:"is_category(array(1,3,10,11,12,13))||is_single() && in_category(array(1,3,10,11,12,13))||is_tag()||is_search()";s:24:"hemingway_video_widget-2";s:45:"is_category(1)||is_single() && in_category(1)";s:8:"search-2";s:110:"is_category(array(1,3,10,11,12,13))||is_single() && in_category(array(1,3,10,11,12,13))||is_tag()||is_search()";s:16:"category-posts-5";s:45:"is_category(2)||is_single() && in_category(2)";s:25:"displaycategorieswidget-2";s:110:"is_category(array(1,3,10,11,12,13))||is_single() && in_category(array(1,3,10,11,12,13))||is_tag()||is_search()";}', 'yes'),
(301, 'mkrdip_cat_post_thumb_sizes', 'a:4:{s:16:"category-posts-2";a:2:{i:0;s:2:"20";i:1;s:2:"20";}s:16:"category-posts-3";a:2:{i:0;s:0:"";i:1;s:0:"";}s:16:"category-posts-4";a:2:{i:0;s:0:"";i:1;s:0:"";}s:16:"category-posts-5";a:2:{i:0;s:0:"";i:1;s:0:"";}}', 'yes'),
(302, 'widget_category-posts', 'a:5:{i:2;a:9:{s:5:"title";s:19:"Liste des tutoriels";s:3:"cat";s:1:"5";s:3:"num";s:1:"0";s:7:"sort_by";s:4:"date";s:10:"title_link";s:2:"on";s:14:"excerpt_length";s:0:"";s:5:"thumb";s:2:"on";s:7:"thumb_w";s:2:"20";s:7:"thumb_h";s:2:"20";}i:3;a:9:{s:5:"title";s:17:"Articles récents";s:3:"cat";s:1:"1";s:3:"num";s:1:"0";s:7:"sort_by";s:4:"date";s:10:"title_link";s:2:"on";s:14:"excerpt_length";s:0:"";s:4:"date";s:2:"on";s:7:"thumb_w";s:0:"";s:7:"thumb_h";s:0:"";}i:4;a:8:{s:5:"title";s:22:"Liste des publications";s:3:"cat";s:1:"4";s:3:"num";s:1:"0";s:7:"sort_by";s:4:"date";s:10:"title_link";s:2:"on";s:14:"excerpt_length";s:0:"";s:7:"thumb_w";s:0:"";s:7:"thumb_h";s:0:"";}i:5;a:8:{s:5:"title";s:21:"Liste des SlideShares";s:3:"cat";s:1:"2";s:3:"num";s:1:"0";s:7:"sort_by";s:4:"date";s:10:"title_link";s:2:"on";s:14:"excerpt_length";s:0:"";s:7:"thumb_w";s:0:"";s:7:"thumb_h";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(214, 'swpsmtp_options', 'a:3:{s:16:"from_email_field";s:23:"postmaster@mnemotix.com";s:15:"from_name_field";s:8:"MNEMOTIX";s:13:"smtp_settings";a:6:{s:4:"host";s:17:"smtp.mnemotix.com";s:15:"type_encryption";s:3:"ssl";s:4:"port";s:3:"587";s:13:"autentication";s:3:"yes";s:8:"username";s:10:"postmaster";s:8:"password";s:8:"yKaKqvmJ";}}', 'yes'),
(577, 'ossdl_off_exclude', '.php', 'yes'),
(359, 'widget_youtubechannelgallery_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(579, 'wpsupercache_start', '1415114539', 'yes');
INSERT INTO `mnx_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(7931, 'rewrite_rules', 'a:72:{s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:47:"themes/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:42:"themes/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:35:"themes/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:17:"themes/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:31:".+?/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:41:".+?/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:61:".+?/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:56:".+?/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:56:".+?/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:26:"(.+?)/([^/]+)/trackback/?$";s:57:"index.php?category_name=$matches[1]&name=$matches[2]&tb=1";s:46:"(.+?)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:69:"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]";s:41:"(.+?)/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:69:"index.php?category_name=$matches[1]&name=$matches[2]&feed=$matches[3]";s:34:"(.+?)/([^/]+)/page/?([0-9]{1,})/?$";s:70:"index.php?category_name=$matches[1]&name=$matches[2]&paged=$matches[3]";s:41:"(.+?)/([^/]+)/comment-page-([0-9]{1,})/?$";s:70:"index.php?category_name=$matches[1]&name=$matches[2]&cpage=$matches[3]";s:26:"(.+?)/([^/]+)(/[0-9]+)?/?$";s:69:"index.php?category_name=$matches[1]&name=$matches[2]&page=$matches[3]";s:20:".+?/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:30:".+?/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:50:".+?/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:45:".+?/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:45:".+?/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:38:"(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:33:"(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:26:"(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:33:"(.+?)/comment-page-([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&cpage=$matches[2]";s:8:"(.+?)/?$";s:35:"index.php?category_name=$matches[1]";}', 'yes'),
(7500, 'category_children', 'a:3:{i:1;a:8:{i:0;i:4;i:1;i:5;i:2;i:10;i:3;i:30;i:4;i:31;i:5;i:32;i:6;i:33;i:7;i:34;}i:33;a:4:{i:0;i:11;i:1;i:12;i:2;i:13;i:3;i:36;}i:34;a:1:{i:0;i:35;}}', 'yes'),
(575, 'ossdl_off_cdn_url', 'http://mnemotix.com/live', 'yes'),
(576, 'ossdl_off_include_dirs', 'wp-content,wp-includes', 'yes'),
(580, 'wpsupercache_count', '0', 'yes'),
(581, 'supercache_stats', 'a:3:{s:9:"generated";i:1415122894;s:10:"supercache";a:5:{s:7:"expired";i:0;s:12:"expired_list";a:0:{}s:6:"cached";i:0;s:11:"cached_list";a:0:{}s:2:"ts";i:1415122894;}s:7:"wpcache";a:3:{s:6:"cached";i:0;s:7:"expired";i:0;s:5:"fsize";s:3:"0KB";}}', 'yes'),
(584, 'preload_cache_counter', 'a:2:{s:1:"c";i:0;s:1:"t";i:1415115494;}', 'yes'),
(583, 'wpsupercache_gc_time', '1415121848', 'yes'),
(816, 'yarpp_version', '4.2.5', 'yes'),
(863, 'yarpp_version_info_timeout', '1432130298', 'no'),
(590, 'wpsmy_combine_js', 'on', 'yes'),
(591, 'wpsmy_combine_css', 'on', 'yes'),
(815, 'yarpp_pro', 'a:7:{s:6:"active";s:1:"0";s:3:"aid";N;s:2:"st";N;s:1:"v";N;s:4:"dpid";N;s:5:"optin";b:0;s:23:"auto_display_post_types";a:1:{i:0;s:4:"post";}}', 'yes'),
(593, 'bwp_minify_general', 'a:24:{s:12:"input_minurl";s:0:"";s:13:"input_minpath";s:0:"";s:15:"input_cache_dir";s:0:"";s:14:"input_doc_root";s:0:"";s:14:"input_maxfiles";s:2:"10";s:12:"input_maxage";s:1:"1";s:12:"input_ignore";s:38:"admin-bar\r\njquery-core\r\njquery-migrate";s:12:"input_header";s:0:"";s:12:"input_direct";s:0:"";s:12:"input_footer";s:0:"";s:14:"input_oblivion";s:0:"";s:18:"input_style_ignore";s:20:"admin-bar\r\ndashicons";s:18:"input_style_direct";s:0:"";s:20:"input_style_oblivion";s:0:"";s:19:"input_custom_buster";s:0:"";s:13:"enable_min_js";s:3:"yes";s:14:"enable_min_css";s:3:"yes";s:15:"enable_bloginfo";s:0:"";s:22:"enable_external_origin";s:0:"";s:17:"enable_css_bubble";s:3:"yes";s:22:"enable_cache_file_lock";s:3:"yes";s:12:"enable_debug";s:0:"";s:18:"select_buster_type";s:4:"none";s:16:"select_time_type";s:5:"86400";}', 'yes'),
(596, 'bwp_minify_advanced', 'a:8:{s:17:"input_fly_minpath";s:0:"";s:23:"input_nginx_config_file";s:0:"";s:14:"input_cdn_host";s:0:"";s:17:"input_cdn_host_js";s:0:"";s:18:"input_cdn_host_css";s:0:"";s:14:"enable_fly_min";s:0:"";s:10:"enable_cdn";s:0:"";s:19:"select_cdn_ssl_type";s:3:"off";}', 'yes'),
(594, 'bwp_minify_detector_log', 'a:3:{s:7:"scripts";a:9:{s:13:"comment-reply";a:8:{s:6:"handle";s:13:"comment-reply";s:3:"src";s:40:"live/wp-includes/js/comment-reply.min.js";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in footer";s:5:"order";i:4;s:5:"group";s:13:"comment-reply";}s:11:"jquery-core";a:8:{s:6:"handle";s:11:"jquery-core";s:3:"src";s:32:"/wp-includes/js/jquery/jquery.js";s:3:"min";b:0;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:17:"ignored in header";s:5:"order";i:3;s:5:"group";s:11:"jquery-core";}s:14:"jquery-migrate";a:8:{s:6:"handle";s:14:"jquery-migrate";s:3:"src";s:44:"/wp-includes/js/jquery/jquery-migrate.min.js";s:3:"min";b:0;s:2:"wp";b:0;s:6:"depend";a:1:{i:0;s:11:"jquery-core";}s:8:"position";s:17:"ignored in header";s:5:"order";i:3;s:5:"group";s:14:"jquery-migrate";}s:11:"jquery-form";a:8:{s:6:"handle";s:11:"jquery-form";s:3:"src";s:69:"live/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";a:1:{i:0;s:6:"jquery";}s:8:"position";s:18:"minified in footer";s:5:"order";i:4;s:5:"group";s:13:"comment-reply";}s:14:"contact-form-7";a:8:{s:6:"handle";s:14:"contact-form-7";s:3:"src";s:61:"live/wp-content/plugins/contact-form-7/includes/js/scripts.js";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";a:2:{i:0;s:6:"jquery";i:1;s:11:"jquery-form";}s:8:"position";s:18:"minified in footer";s:5:"order";i:4;s:5:"group";s:13:"comment-reply";}s:16:"hemingway_global";a:8:{s:6:"handle";s:16:"hemingway_global";s:3:"src";s:39:"live/wp-content/themes/mnx/js/global.js";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";a:1:{i:0;s:6:"jquery";}s:8:"position";s:18:"minified in footer";s:5:"order";i:4;s:5:"group";s:16:"hemingway_global";}s:18:"youtube_player_api";a:8:{s:6:"handle";s:18:"youtube_player_api";s:3:"src";s:34:"https://www.youtube.com/player_api";s:3:"min";b:0;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:17:"ignored in footer";s:5:"order";i:5;s:5:"group";s:18:"youtube_player_api";}s:23:"youtube-channel-gallery";a:8:{s:6:"handle";s:23:"youtube-channel-gallery";s:3:"src";s:58:"live/wp-content/plugins/youtube-channel-gallery/scripts.js";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in footer";s:5:"order";i:4;s:5:"group";s:13:"comment-reply";}s:9:"admin-bar";a:8:{s:6:"handle";s:9:"admin-bar";s:3:"src";s:32:"/wp-includes/js/admin-bar.min.js";s:3:"min";b:0;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:17:"ignored in footer";s:5:"order";i:5;s:5:"group";s:9:"admin-bar";}}s:6:"styles";a:13:{s:14:"category-posts";a:9:{s:6:"handle";s:14:"category-posts";s:3:"src";s:52:"live/wp-content/plugins/category-posts/cat-posts.css";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in header";s:5:"order";i:2;s:5:"group";s:14:"yarppWidgetCss";s:5:"media";s:3:"all";}s:14:"contact-form-7";a:9:{s:6:"handle";s:14:"contact-form-7";s:3:"src";s:62:"live/wp-content/plugins/contact-form-7/includes/css/styles.css";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in header";s:5:"order";i:2;s:5:"group";s:14:"yarppWidgetCss";s:5:"media";s:3:"all";}s:15:"hemingway_style";a:9:{s:6:"handle";s:15:"hemingway_style";s:3:"src";s:36:"live/wp-content/themes/mnx/style.css";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in header";s:5:"order";i:2;s:5:"group";s:14:"yarppWidgetCss";s:5:"media";s:3:"all";}s:23:"youtube-channel-gallery";a:9:{s:6:"handle";s:23:"youtube-channel-gallery";s:3:"src";s:58:"live/wp-content/plugins/youtube-channel-gallery/styles.css";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in footer";s:5:"order";i:4;s:5:"group";s:23:"youtube-channel-gallery";s:5:"media";s:3:"all";}s:7:"buttons";a:9:{s:6:"handle";s:7:"buttons";s:3:"src";s:36:"live/wp-includes/css/buttons.min.css";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in header";s:5:"order";i:2;s:5:"group";s:7:"buttons";s:5:"media";s:3:"all";}s:9:"open-sans";a:9:{s:6:"handle";s:9:"open-sans";s:3:"src";s:108:"//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,300,400,600&subset=latin,latin-ext";s:3:"min";b:0;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:17:"ignored in header";s:5:"order";i:3;s:5:"group";s:9:"open-sans";s:5:"media";s:3:"all";}s:9:"dashicons";a:9:{s:6:"handle";s:9:"dashicons";s:3:"src";s:34:"/wp-includes/css/dashicons.min.css";s:3:"min";b:0;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:17:"ignored in header";s:5:"order";i:3;s:5:"group";s:9:"dashicons";s:5:"media";s:3:"all";}s:5:"login";a:9:{s:6:"handle";s:5:"login";s:3:"src";s:31:"live/wp-admin/css/login.min.css";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";a:3:{i:0;s:7:"buttons";i:1;s:9:"open-sans";i:2;s:9:"dashicons";}s:8:"position";s:18:"minified in header";s:5:"order";i:2;s:5:"group";s:7:"buttons";s:5:"media";s:3:"all";}s:9:"admin-bar";a:9:{s:6:"handle";s:9:"admin-bar";s:3:"src";s:34:"/wp-includes/css/admin-bar.min.css";s:3:"min";b:0;s:2:"wp";b:0;s:6:"depend";a:2:{i:0;s:9:"open-sans";i:1;s:9:"dashicons";}s:8:"position";s:17:"ignored in header";s:5:"order";i:3;s:5:"group";s:9:"admin-bar";s:5:"media";s:3:"all";}s:5:"boxes";a:9:{s:6:"handle";s:5:"boxes";s:3:"src";s:58:"live/wp-content/plugins/wordpress-seo/css/adminbar.min.css";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in header";s:5:"order";i:2;s:5:"group";s:14:"yarppWidgetCss";s:5:"media";s:3:"all";}s:14:"yarppWidgetCss";a:9:{s:6:"handle";s:14:"yarppWidgetCss";s:3:"src";s:73:"live/wp-content/plugins/yet-another-related-posts-plugin/style/widget.css";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in header";s:5:"order";i:2;s:5:"group";s:14:"yarppWidgetCss";s:5:"media";s:3:"all";}s:15:"yarppRelatedCss";a:9:{s:6:"handle";s:15:"yarppRelatedCss";s:3:"src";s:74:"live/wp-content/plugins/yet-another-related-posts-plugin/style/related.css";s:3:"min";b:1;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:18:"minified in footer";s:5:"order";i:4;s:5:"group";s:15:"yarppRelatedCss";s:5:"media";s:3:"all";}s:32:"yarpp-thumbnails-yarpp-thumbnail";a:9:{s:6:"handle";s:32:"yarpp-thumbnails-yarpp-thumbnail";s:3:"src";s:131:"http://mnemotix.com/live/wp-content/plugins/yet-another-related-posts-plugin/includes/styles_thumbnails.css.php?width=120&height=90";s:3:"min";b:0;s:2:"wp";b:0;s:6:"depend";b:0;s:8:"position";s:17:"ignored in footer";s:5:"order";i:5;s:5:"group";s:32:"yarpp-thumbnails-yarpp-thumbnail";s:5:"media";s:3:"all";}}s:6:"groups";a:0:{}}', 'yes'),
(595, 'bwp_minify_version', '1.3.3', 'yes'),
(630, 'backwpup_cfg_hash', '3d434d', 'yes'),
(631, 'backwpup_jobs', 'a:1:{i:1;a:45:{s:5:"jobid";i:1;s:4:"type";a:4:{i:0;s:6:"DBDUMP";i:1;s:4:"FILE";i:2;s:5:"WPEXP";i:3;s:8:"WPPLUGIN";}s:12:"destinations";a:1:{i:0;s:7:"DROPBOX";}s:4:"name";s:13:"BackUpDropbox";s:14:"mailaddresslog";s:18:"pymichel@gmail.com";s:20:"mailaddresssenderlog";s:38:"BackWPup Mnemotix <pymichel@gmail.com>";s:13:"mailerroronly";b:1;s:10:"backuptype";s:7:"archive";s:13:"archiveformat";s:7:".tar.gz";s:11:"archivename";s:33:"backwpup_3d434d_%Y-%m-%d_%H-%i-%s";s:7:"lastrun";i:1439101637;s:7:"logfile";s:111:"/home/mnemotix/www/live/wp-content/uploads/backwpup-3d434d-logs/backwpup_log_3d434d_2015-08-09_06-27-17.html.gz";s:21:"lastbackupdownloadurl";s:0:"";s:11:"lastruntime";i:18;s:10:"activetype";s:6:"wpcron";s:10:"cronselect";s:5:"basic";s:4:"cron";s:10:"30 3 * * 0";s:21:"dbdumpfilecompression";s:3:".gz";s:10:"dbdumpfile";s:8:"mnemotix";s:13:"dbdumpexclude";a:0:{}s:11:"fileexclude";s:36:".DS_Store,.git,.svn,.tmp,desktop.ini";s:10:"dirinclude";s:0:"";s:19:"backupexcludethumbs";b:0;s:18:"backupspecialfiles";b:1;s:10:"backuproot";b:1;s:21:"backuprootexcludedirs";a:0:{}s:13:"backupcontent";b:1;s:24:"backupcontentexcludedirs";a:2:{i:0;s:5:"cache";i:1;s:7:"upgrade";}s:13:"backupplugins";b:1;s:24:"backuppluginsexcludedirs";a:1:{i:0;s:8:"backwpup";}s:12:"backupthemes";b:1;s:23:"backupthemesexcludedirs";a:3:{i:0;s:14:"twentyfourteen";i:1;s:14:"twentythirteen";i:2;s:12:"twentytwelve";}s:13:"backupuploads";b:1;s:24:"backupuploadsexcludedirs";a:1:{i:0;s:20:"backwpup-3d434d-logs";}s:15:"wpexportcontent";s:3:"all";s:12:"wpexportfile";s:27:"Mnemotix.wordpress.%Y-%m-%d";s:23:"wpexportfilecompression";s:3:".gz";s:14:"pluginlistfile";s:28:"Mnemotix.pluginlist.%Y-%m-%d";s:25:"pluginlistfilecompression";s:3:".gz";s:12:"dropboxtoken";s:16:"mp7ztsty3sv2ymd5";s:13:"dropboxsecret";s:63:"$BackWPup$RIJNDAEL$vyOcsaLyh84WXI9yU5eeWG64MF5VBOHIvJ5/KWfMAT4=";s:11:"dropboxroot";s:7:"dropbox";s:19:"dropboxsyncnodelete";b:0;s:17:"dropboxmaxbackups";i:10;s:10:"dropboxdir";s:9:"Mnemotix/";}}', 'no'),
(7401, '_transient_timeout_feed_66a70e9599b658d5cc038e8074597e7c', '1437524591', 'no'),
(632, 'backwpup_version', '3.1.4-inactive', 'yes'),
(667, 'backwpup_cfg_logfolder', '/home/mnemotix/www/live/wp-content/uploads/backwpup-3d434d-logs/', 'yes'),
(666, 'backwpup_cfg_gzlogs', '1', 'yes'),
(658, 'backwpup_cfg_jobmaxexecutiontime', '0', 'yes'),
(659, 'backwpup_cfg_jobziparchivemethod', '', 'yes'),
(660, 'backwpup_cfg_jobstepretry', '3', 'yes'),
(661, 'backwpup_cfg_jobsteprestart', '0', 'yes'),
(662, 'backwpup_cfg_jobrunauthkey', '00f9311d', 'yes'),
(663, 'backwpup_cfg_jobnotranslate', '0', 'yes'),
(664, 'backwpup_cfg_jobwaittimems', '0', 'yes'),
(665, 'backwpup_cfg_maxlogs', '30', 'yes'),
(655, 'backwpup_cfg_showadminbar', '0', 'yes'),
(656, 'backwpup_cfg_showfoldersize', '0', 'yes'),
(657, 'backwpup_cfg_protectfolders', '1', 'yes'),
(649, 'backwpup_about_page', '1', 'yes'),
(668, 'backwpup_cfg_httpauthuser', '', 'yes'),
(669, 'backwpup_cfg_httpauthpassword', '', 'yes'),
(670, 'backwpup_messages', 'a:0:{}', 'yes'),
(5195, 'db_upgraded', '', 'yes'),
(1085, 'auto_core_update_notified', 'a:4:{s:4:"type";s:7:"success";s:5:"email";s:18:"pymichel@gmail.com";s:7:"version";s:5:"4.2.6";s:9:"timestamp";i:1452184281;}', 'yes'),
(818, 'yarpp', 'a:47:{s:9:"threshold";s:1:"4";s:5:"limit";s:1:"4";s:14:"excerpt_length";s:2:"10";s:6:"recent";b:0;s:12:"before_title";s:4:"<li>";s:11:"after_title";s:5:"</li>";s:11:"before_post";s:8:" <small>";s:10:"after_post";s:8:"</small>";s:14:"before_related";s:27:"<h3>Related posts:</h3><ol>";s:13:"after_related";s:5:"</ol>";s:10:"no_results";s:33:"<p>Pas d''articles similaires.</p>";s:5:"order";s:10:"score DESC";s:9:"rss_limit";s:1:"3";s:18:"rss_excerpt_length";s:2:"10";s:16:"rss_before_title";s:4:"<li>";s:15:"rss_after_title";s:5:"</li>";s:15:"rss_before_post";s:8:" <small>";s:14:"rss_after_post";s:8:"</small>";s:18:"rss_before_related";s:27:"<h3>Related posts:</h3><ol>";s:17:"rss_after_related";s:5:"</ol>";s:14:"rss_no_results";s:24:"<p>No related posts.</p>";s:9:"rss_order";s:10:"score DESC";s:9:"past_only";b:0;s:12:"show_excerpt";b:1;s:16:"rss_show_excerpt";b:0;s:8:"template";s:10:"thumbnails";s:12:"rss_template";b:0;s:14:"show_pass_post";b:0;s:12:"cross_relate";b:0;s:11:"rss_display";b:0;s:19:"rss_excerpt_display";b:1;s:13:"promote_yarpp";b:0;s:17:"rss_promote_yarpp";b:0;s:15:"myisam_override";b:0;s:7:"exclude";s:5:"4,2,5";s:6:"weight";a:3:{s:5:"title";i:1;s:4:"body";i:1;s:3:"tax";a:2:{s:8:"category";i:3;s:8:"post_tag";i:1;}}s:11:"require_tax";a:0:{}s:5:"optin";b:0;s:18:"thumbnails_heading";s:21:"Articles similaires :";s:18:"thumbnails_default";s:63:"http://mnemotix.com/live/wp-content/uploads/2014/10/logoRVB.png";s:22:"rss_thumbnails_heading";s:14:"Related posts:";s:22:"rss_thumbnails_default";s:95:"http://mnemotix.com/live/wp-content/plugins/yet-another-related-posts-plugin/images/default.png";s:12:"display_code";b:0;s:20:"auto_display_archive";b:0;s:23:"auto_display_post_types";a:0:{}s:5:"pools";a:0:{}s:25:"manually_using_thumbnails";b:0;}', 'yes'),
(3894, '_transient_wpseo_sitemap_cache_post_', '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n	<url>\n		<loc>http://mnemotix.com</loc>\n		<lastmod>2015-05-19T11:37:41+01:00</lastmod>\n		<changefreq>daily</changefreq>\n		<priority>1</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/blogtemp/linked-open-data/bonjour-tout-le-monde/</loc>\n		<lastmod>2014-11-10T17:23:05+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/blogtemp/linked-open-data/drink-more-coffee/</loc>\n		<lastmod>2014-11-11T10:30:21+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n		<image:image>\n			<image:loc>http://mnemotix.com/live/wp-content/uploads/2014/10/process.png</image:loc>\n			<image:caption><![CDATA[Drink more coffee]]></image:caption>\n		</image:image>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/blogtemp/news/olor-remped-modis-volor/</loc>\n		<lastmod>2014-11-10T17:08:50+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n		<image:image>\n			<image:loc>http://mnemotix.com/live/wp-content/uploads/2014/10/recto-apercu.jpg</image:loc>\n			<image:caption><![CDATA[Olor remped modis volor]]></image:caption>\n		</image:image>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/tutoriels/mon-premier-tuto/</loc>\n		<lastmod>2014-10-24T15:25:23+01:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n		<image:image>\n			<image:loc>http://mnemotix.com/live/wp-content/uploads/2014/10/process.png</image:loc>\n			<image:caption><![CDATA[Mon premier tuto.]]></image:caption>\n		</image:image>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/tutoriels/tutoriel-2/</loc>\n		<lastmod>2014-10-24T15:27:01+01:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n		<image:image>\n			<image:loc>http://mnemotix.com/live/wp-content/uploads/2014/10/process.png</image:loc>\n			<image:caption><![CDATA[tutoriel 2]]></image:caption>\n		</image:image>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/tutoriels/le-tuto-de-toto-3/</loc>\n		<lastmod>2014-10-24T15:26:28+01:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n		<image:image>\n			<image:loc>http://mnemotix.com/live/wp-content/uploads/2014/10/process.png</image:loc>\n			<image:caption><![CDATA[le tuto de toto 3]]></image:caption>\n		</image:image>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/publications/publication-1/</loc>\n		<lastmod>2014-11-02T13:38:04+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/publications/publication-2/</loc>\n		<lastmod>2014-11-02T13:37:56+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/slideshares/economie-collaborative-mode-tendance-transition-durable/</loc>\n		<lastmod>2014-10-28T14:48:19+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/slideshares/sophiaconf-2014-semantic-web-inside/</loc>\n		<lastmod>2014-11-02T13:36:11+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/blogtemp/news/nouveau-test/</loc>\n		<lastmod>2014-11-28T14:43:55+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.6</priority>\n	</url>\n</urlset>', 'no'),
(3892, '_transient_wpseo_sitemap_cache_page_', '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n	<url>\n		<loc>http://mnemotix.com</loc>\n		<lastmod>2015-05-20T11:49:49+01:00</lastmod>\n		<changefreq>daily</changefreq>\n		<priority>1</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/videos/</loc>\n		<lastmod>2014-10-27T16:51:50+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.8</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/contact-form-test/</loc>\n		<lastmod>2014-10-22T13:00:58+01:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.8</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/slides/</loc>\n		<lastmod>2014-10-27T16:54:35+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.8</priority>\n	</url>\n</urlset>', 'no'),
(6051, 'yarpp_upgraded', '1', 'yes'),
(5200, '_transient_wpseo_sitemap_cache_category_', '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n	<url>\n		<loc>http://mnemotix.com/blogtemp/</loc>\n		<lastmod>2015-05-19T14:59:17+01:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.2</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/blogtemp/linked-open-data/</loc>\n		<lastmod>2014-11-11T10:30:21+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.2</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/blogtemp/news/</loc>\n		<lastmod>2014-11-28T14:43:55+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.2</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/blogtemp/open-data/</loc>\n		<lastmod>2014-11-10T17:08:50+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.2</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/publications/</loc>\n		<lastmod>2014-11-02T13:38:04+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.2</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/slideshares/</loc>\n		<lastmod>2014-11-02T13:36:11+00:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.2</priority>\n	</url>\n	<url>\n		<loc>http://mnemotix.com/tutoriels/</loc>\n		<lastmod>2014-10-24T15:27:01+01:00</lastmod>\n		<changefreq>weekly</changefreq>\n		<priority>0.2</priority>\n	</url>\n</urlset>', 'no'),
(9844, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1454358843;s:7:"checked";a:1:{s:3:"mnx";s:0:"";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'yes'),
(10551, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:4:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:65:"https://downloads.wordpress.org/release/fr_FR/wordpress-4.4.1.zip";s:6:"locale";s:5:"fr_FR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/fr_FR/wordpress-4.4.1.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.4.1";s:7:"version";s:5:"4.4.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.4";s:15:"partial_version";s:0:"";}i:1;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.4.1.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.4.1.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.4.1-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.4.1-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.4.1";s:7:"version";s:5:"4.4.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.4";s:15:"partial_version";s:0:"";}i:2;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.4.1.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.4.1.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.4.1-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.4.1-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.4.1";s:7:"version";s:5:"4.4.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.4";s:15:"partial_version";s:0:"";s:9:"new_files";s:1:"1";}i:3;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.3.2.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.3.2.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.3.2-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.3.2-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.3.2";s:7:"version";s:5:"4.3.2";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.4";s:15:"partial_version";s:0:"";s:9:"new_files";s:1:"1";}}s:12:"last_checked";i:1454358839;s:15:"version_checked";s:5:"4.2.6";s:12:"translations";a:0:{}}', 'yes');

DROP TABLE IF EXISTS `mnx_postmeta`;
CREATE TABLE IF NOT EXISTS `mnx_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=MyISAM AUTO_INCREMENT=421 DEFAULT CHARSET=utf8;

INSERT INTO `mnx_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(4, 5, '_wp_attached_file', '2014/10/logoRVB.png'),
(5, 5, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:400;s:6:"height";i:400;s:4:"file";s:19:"2014/10/logoRVB.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"logoRVB-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"logoRVB-300x300.png";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(6, 6, '_wp_attached_file', '2014/10/logo.png'),
(7, 6, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:726;s:6:"height";i:900;s:4:"file";s:16:"2014/10/logo.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"logo-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:16:"logo-242x300.png";s:5:"width";i:242;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:10:"post-image";a:4:{s:4:"file";s:16:"logo-676x838.png";s:5:"width";i:676;s:6:"height";i:838;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(8, 7, '_wp_attached_file', '2014/10/recto-apercu.jpg'),
(9, 7, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:723;s:6:"height";i:459;s:4:"file";s:24:"2014/10/recto-apercu.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"recto-apercu-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"recto-apercu-300x190.jpg";s:5:"width";i:300;s:6:"height";i:190;s:9:"mime-type";s:10:"image/jpeg";}s:10:"post-image";a:4:{s:4:"file";s:24:"recto-apercu-676x429.jpg";s:5:"width";i:676;s:6:"height";i:429;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(10, 8, '_wp_attached_file', '2014/10/process.png'),
(11, 8, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1130;s:6:"height";i:932;s:4:"file";s:19:"2014/10/process.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"process-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"process-300x247.png";s:5:"width";i:300;s:6:"height";i:247;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:20:"process-1024x844.png";s:5:"width";i:1024;s:6:"height";i:844;s:9:"mime-type";s:9:"image/png";}s:10:"post-image";a:4:{s:4:"file";s:19:"process-676x557.png";s:5:"width";i:676;s:6:"height";i:557;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(31, 15, '_form', '<p>Nom (obligatoire)<br />\n    [text* your-name] </p>\n\n<p>E-mail (obligatoire)<br />\n    [email* your-email] </p>\n\n<p>Sujet<br />\n    [text your-subject] </p>\n\n<p>Message<br />\n    [textarea your-message] </p>\n\n<p>[submit "Envoyer"]</p>'),
(32, 15, '_mail', 'a:8:{s:7:"subject";s:14:"[your-subject]";s:6:"sender";s:36:"[your-name] <wordpress@mnemotix.com>";s:4:"body";s:187:"De : [your-name] <[your-email]>\nSujet : [your-subject]\n\nCorps du message :\n[your-message]\n\n--\nCet email a été envoyé via le formulaire de contact de Mnemotix (http://mnemotix.com/live)";s:9:"recipient";s:18:"pymichel@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(33, 15, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:14:"[your-subject]";s:6:"sender";s:33:"Mnemotix <wordpress@mnemotix.com>";s:4:"body";s:131:"Corps du message :\n[your-message]\n\n--\nCet email a été envoyé via le formulaire de contact de Mnemotix (http://mnemotix.com/live)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:28:"Reply-To: pymichel@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(34, 15, '_messages', 'a:21:{s:12:"mail_sent_ok";s:42:"Votre message a bien été envoyé. Merci.";s:12:"mail_sent_ng";s:116:"Erreur lors de l''envoi du message. Veuillez réessayer plus tard ou contacter l''administrateur d''une autre manière.";s:16:"validation_error";s:76:"Erreur de validation. Veuillez vérifier les champs et soumettre à nouveau.";s:4:"spam";s:116:"Erreur lors de l''envoi du message. Veuillez réessayer plus tard ou contacter l''administrateur d''une autre manière.";s:12:"accept_terms";s:61:"Merci de bien vouloir accepter les conditions pour continuer.";s:16:"invalid_required";s:38:"Veuillez remplir le champ obligatoire.";s:17:"captcha_not_match";s:29:"Le code entré est incorrect.";s:14:"invalid_number";s:37:"Le format numérique semble invalide.";s:16:"number_too_small";s:25:"Ce nombre est trop petit.";s:16:"number_too_large";s:25:"Ce nombre est trop grand.";s:13:"invalid_email";s:32:"L''adresse email semble invalide.";s:11:"invalid_url";s:22:"L''URL semble invalide.";s:11:"invalid_tel";s:42:"Le numéro de téléphone semble invalide.";s:23:"quiz_answer_not_correct";s:30:"Votre réponse est incorrecte.";s:12:"invalid_date";s:34:"Le format de date semble invalide.";s:14:"date_too_early";s:25:"Cette date est trop tôt.";s:13:"date_too_late";s:25:"Cette date est trop tard.";s:13:"upload_failed";s:39:"Impossible de télécharger le fichier.";s:24:"upload_file_type_invalid";s:39:"Ce type de fichier n''est pas autorisé.";s:21:"upload_file_too_large";s:31:"Ce fichier est trop volumineux.";s:23:"upload_failed_php_error";s:66:"Impossible de mettre en ligne le fichier. Une erreur est survenue.";}'),
(35, 15, '_additional_settings', ''),
(36, 15, '_locale', 'fr_FR'),
(37, 16, '_form', '<p>Your Name (required)<br />\n    [text* your-name] </p>\n\n<p>Your Email (required)<br />\n    [email* your-email] </p>\n\n<p>Subject<br />\n    [text your-subject] </p>\n\n<p>Your Message<br />\n    [textarea your-message] </p>\n\n<p>[submit "Send"]</p>'),
(38, 16, '_mail', 'a:8:{s:7:"subject";s:14:"[your-subject]";s:6:"sender";s:36:"[your-name] <wordpress@mnemotix.com>";s:4:"body";s:170:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on Mnemotix (http://mnemotix.com/live)";s:9:"recipient";s:18:"pymichel@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(39, 16, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:14:"[your-subject]";s:6:"sender";s:33:"Mnemotix <wordpress@mnemotix.com>";s:4:"body";s:112:"Message Body:\n[your-message]\n\n--\nThis e-mail was sent from a contact form on Mnemotix (http://mnemotix.com/live)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:28:"Reply-To: pymichel@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(40, 16, '_messages', 'a:21:{s:12:"mail_sent_ok";s:43:"Your message was sent successfully. Thanks.";s:12:"mail_sent_ng";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:16:"validation_error";s:74:"Validation errors occurred. Please confirm the fields and submit it again.";s:4:"spam";s:93:"Failed to send your message. Please try later or contact the administrator by another method.";s:12:"accept_terms";s:35:"Please accept the terms to proceed.";s:16:"invalid_required";s:31:"Please fill the required field.";s:17:"captcha_not_match";s:31:"Your entered code is incorrect.";s:14:"invalid_number";s:28:"Number format seems invalid.";s:16:"number_too_small";s:25:"This number is too small.";s:16:"number_too_large";s:25:"This number is too large.";s:13:"invalid_email";s:28:"Email address seems invalid.";s:11:"invalid_url";s:18:"URL seems invalid.";s:11:"invalid_tel";s:31:"Telephone number seems invalid.";s:23:"quiz_answer_not_correct";s:27:"Your answer is not correct.";s:12:"invalid_date";s:26:"Date format seems invalid.";s:14:"date_too_early";s:23:"This date is too early.";s:13:"date_too_late";s:22:"This date is too late.";s:13:"upload_failed";s:22:"Failed to upload file.";s:24:"upload_file_type_invalid";s:30:"This file type is not allowed.";s:21:"upload_file_too_large";s:23:"This file is too large.";s:23:"upload_failed_php_error";s:38:"Failed to upload file. Error occurred.";}'),
(41, 16, '_additional_settings', ''),
(42, 16, '_locale', 'en_US'),
(402, 121, '_edit_lock', '1437505413:2'),
(391, 118, '_edit_lock', '1437505378:2'),
(392, 118, '_edit_last', '2'),
(395, 118, 'videourl', ''),
(403, 121, '_edit_last', '2'),
(387, 116, '_edit_last', '2'),
(388, 116, 'videourl', ''),
(386, 116, '_edit_lock', '1437505484:2'),
(406, 121, 'videourl', ''),
(319, 101, '_menu_item_type', 'taxonomy'),
(320, 101, '_menu_item_menu_item_parent', '0'),
(321, 101, '_menu_item_object_id', '11'),
(322, 101, '_menu_item_object', 'category'),
(323, 101, '_menu_item_target', ''),
(324, 101, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(325, 101, '_menu_item_xfn', ''),
(326, 101, '_menu_item_url', ''),
(328, 102, '_menu_item_type', 'taxonomy'),
(329, 102, '_menu_item_menu_item_parent', '0'),
(330, 102, '_menu_item_object_id', '12'),
(331, 102, '_menu_item_object', 'category'),
(332, 102, '_menu_item_target', ''),
(333, 102, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(334, 102, '_menu_item_xfn', ''),
(335, 102, '_menu_item_url', '');

DROP TABLE IF EXISTS `mnx_posts`;
CREATE TABLE IF NOT EXISTS `mnx_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

INSERT INTO `mnx_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(5, 1, '2014-10-20 15:14:19', '2014-10-20 14:14:19', '', 'logoRVB', '', 'inherit', 'open', 'open', '', 'logorvb', '', '', '2014-10-20 15:14:19', '2014-10-20 14:14:19', '', 0, 'http://mnemotix.com/live/wp-content/uploads/2014/10/logoRVB.png', 0, 'attachment', 'image/png', 0),
(6, 1, '2014-10-20 15:14:23', '2014-10-20 14:14:23', '', 'logo', '', 'inherit', 'open', 'open', '', 'logo', '', '', '2014-10-20 15:14:23', '2014-10-20 14:14:23', '', 0, 'http://mnemotix.com/live/wp-content/uploads/2014/10/logo.png', 0, 'attachment', 'image/png', 0),
(7, 1, '2014-10-20 15:14:55', '2014-10-20 14:14:55', '', 'carte', '', 'inherit', 'open', 'open', '', 'recto-apercu', '', '', '2014-10-22 13:59:26', '2014-10-22 12:59:26', '', 0, 'http://mnemotix.com/live/wp-content/uploads/2014/10/recto-apercu.jpg', 0, 'attachment', 'image/jpeg', 0),
(8, 1, '2014-10-20 15:16:25', '2014-10-20 14:16:25', '', 'process', '', 'inherit', 'open', 'open', '', 'process', '', '', '2014-10-20 15:16:25', '2014-10-20 14:16:25', '', 0, 'http://mnemotix.com/live/wp-content/uploads/2014/10/process.png', 0, 'attachment', 'image/png', 0),
(15, 1, '2014-10-21 10:18:07', '2014-10-21 09:18:07', '<p>Nom (obligatoire)<br />\r\n    [text* your-name] </p>\r\n\r\n<p>E-mail (obligatoire)<br />\r\n    [email* your-email] </p>\r\n\r\n<p>Sujet<br />\r\n    [text your-subject] </p>\r\n\r\n<p>Message<br />\r\n    [textarea your-message] </p>\r\n\r\n<p>[submit "Envoyer"]</p>\n[your-subject]\n[your-name] <wordpress@mnemotix.com>\nDe : [your-name] <[your-email]>\r\nSujet : [your-subject]\r\n\r\nCorps du message :\r\n[your-message]\r\n\r\n--\r\nCet email a été envoyé via le formulaire de contact de Mnemotix (http://mnemotix.com/live)\npymichel@gmail.com\nReply-To: [your-email]\n\n\n\n\n[your-subject]\nMnemotix <wordpress@mnemotix.com>\nCorps du message :\r\n[your-message]\r\n\r\n--\r\nCet email a été envoyé via le formulaire de contact de Mnemotix (http://mnemotix.com/live)\n[your-email]\nReply-To: pymichel@gmail.com\n\n\n\nVotre message a bien été envoyé. Merci.\nErreur lors de l''envoi du message. Veuillez réessayer plus tard ou contacter l''administrateur d''une autre manière.\nErreur de validation. Veuillez vérifier les champs et soumettre à nouveau.\nErreur lors de l''envoi du message. Veuillez réessayer plus tard ou contacter l''administrateur d''une autre manière.\nMerci de bien vouloir accepter les conditions pour continuer.\nVeuillez remplir le champ obligatoire.\nLe code entré est incorrect.\nLe format numérique semble invalide.\nCe nombre est trop petit.\nCe nombre est trop grand.\nL''adresse email semble invalide.\nL''URL semble invalide.\nLe numéro de téléphone semble invalide.\nVotre réponse est incorrecte.\nLe format de date semble invalide.\nCette date est trop tôt.\nCette date est trop tard.\nImpossible de télécharger le fichier.\nCe type de fichier n''est pas autorisé.\nCe fichier est trop volumineux.\nImpossible de mettre en ligne le fichier. Une erreur est survenue.', 'Formulaire de contact', '', 'publish', 'closed', 'open', '', 'formulaire-de-contact-1', '', '', '2014-10-22 14:02:05', '2014-10-22 13:02:05', '', 0, 'http://mnemotix.com/live/?post_type=wpcf7_contact_form&#038;p=15', 0, 'wpcf7_contact_form', '', 0),
(16, 1, '2014-10-21 10:21:05', '2014-10-21 09:21:05', '<p>Your Name (required)<br />\r\n    [text* your-name] </p>\r\n\r\n<p>Your Email (required)<br />\r\n    [email* your-email] </p>\r\n\r\n<p>Subject<br />\r\n    [text your-subject] </p>\r\n\r\n<p>Your Message<br />\r\n    [textarea your-message] </p>\r\n\r\n<p>[submit "Send"]</p>\n[your-subject]\n[your-name] <wordpress@mnemotix.com>\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n--\r\nThis e-mail was sent from a contact form on Mnemotix (http://mnemotix.com/live)\npymichel@gmail.com\nReply-To: [your-email]\n\n\n\n\n[your-subject]\nMnemotix <wordpress@mnemotix.com>\nMessage Body:\r\n[your-message]\r\n\r\n--\r\nThis e-mail was sent from a contact form on Mnemotix (http://mnemotix.com/live)\n[your-email]\nReply-To: pymichel@gmail.com\n\n\n\nYour message was sent successfully. Thanks.\nFailed to send your message. Please try later or contact the administrator by another method.\nValidation errors occurred. Please confirm the fields and submit it again.\nFailed to send your message. Please try later or contact the administrator by another method.\nPlease accept the terms to proceed.\nPlease fill the required field.\nYour entered code is incorrect.\nNumber format seems invalid.\nThis number is too small.\nThis number is too large.\nEmail address seems invalid.\nURL seems invalid.\nTelephone number seems invalid.\nYour answer is not correct.\nDate format seems invalid.\nThis date is too early.\nThis date is too late.\nFailed to upload file.\nThis file type is not allowed.\nThis file is too large.\nFailed to upload file. Error occurred.', 'Contact Form', '', 'publish', 'closed', 'open', '', 'contact-form', '', '', '2014-10-21 10:21:05', '2014-10-21 09:21:05', '', 0, 'http://mnemotix.com/live/?post_type=wpcf7_contact_form&p=16', 0, 'wpcf7_contact_form', '', 0),
(122, 3, '2015-07-08 13:32:53', '2015-07-08 12:32:53', 'Dans le cadre des <a href="http://devlog.cnrs.fr/jdev2015">Journées nationales du DEVeloppement logiciel de l''Enseignement Supérieur et Recherche (JDEVs)</a> à Bordeaux, Mnémotix est intervenu pour l''animation de l''atelier : T7.A08 Développement de Single Page Application avec BackboneJS + Marionette : exemple de connexion cross-domain à une API REST. Vous trouverez les tutoriaux réalisés à cet effet ci-dessous :\r\n<ul>\r\n	<li><a href="http://mnemotix.github.io/backbone-tuto/" target="_blank">Apprendre BackboneJS</a> : Créer un client REST complet avec Gulp et Backbone</li>\r\n	<li><a href="http://mnemotix.github.io/gulp-tuto/" target="_blank">Apprendre GULP</a> : créer un projet Gulp avec browserify</li>\r\n</ul>\r\n<h2></h2>', 'Tutoriels Mnémotix', '', 'inherit', 'closed', 'open', '', '121-revision-v1', '', '', '2015-07-08 13:32:53', '2015-07-08 12:32:53', '', 121, 'http://mnemotix.com/blogtemp/121-revision-v1/', 0, 'revision', '', 0),
(123, 3, '2015-07-08 13:33:31', '2015-07-08 12:33:31', 'Dans le cadre des <a href="http://devlog.cnrs.fr/jdev2015">Journées nationales du DEVeloppement logiciel de l''Enseignement Supérieur et Recherche (JDEVs)</a> à Bordeaux, Mnémotix est intervenu pour l''animation de l''atelier : <a href="http://devlog.cnrs.fr/jdev2015/t7.a08" target="_blank">T7.A08 Développement de Single Page Application avec BackboneJS + Marionette : exemple de connexion cross-domain à une API REST</a>. Vous trouverez les tutoriaux réalisés à cet effet ci-dessous :\r\n<ul>\r\n	<li><a href="http://mnemotix.github.io/backbone-tuto/" target="_blank">Apprendre BackboneJS</a> : Créer un client REST complet avec Gulp et Backbone</li>\r\n	<li><a href="http://mnemotix.github.io/gulp-tuto/" target="_blank">Apprendre GULP</a> : créer un projet Gulp avec browserify</li>\r\n</ul>\r\n<h2></h2>', 'Tutoriels Mnémotix', '', 'inherit', 'closed', 'open', '', '121-revision-v1', '', '', '2015-07-08 13:33:31', '2015-07-08 12:33:31', '', 121, 'http://mnemotix.com/blogtemp/121-revision-v1/', 0, 'revision', '', 0),
(118, 3, '2015-07-08 11:18:56', '2015-07-08 10:18:56', 'Après plusieurs mois de travail et de co-construction avec le collectif de coWorking et coEvolution un jour ici... nous voici cet été installés à l''Ademe. Mnémotix se joint à plusieurs structures et entrepreneurs pour proposer l''animation d''un espace de coworking dont les concepts clés sont : gouvernance collaborative, intelligence collective, innovation transformative, économie de la fonctionnalité et de la coopération, développement durable, innovation sociale, modèles d''affaire alternatifs, culture numérique 3.0.\r\n\r\nVenez découvrir le Summer Camp un été ici à l''Ademe Sophia Antipolis\r\n<div class="fb-post" data-href="https://www.facebook.com/unjourici06/posts/864121433669729:0" data-width="500">\r\n<div class="fb-xfbml-parse-ignore">\r\n<blockquote cite="https://www.facebook.com/unjourici06/posts/864121433669729:0">Bonne nouvelle ! un jour ici... s''installe à l''ADEME Sophia Antipolis pour un été ici... incubation de son expérience de coWorking &amp; coEvolution ! Participez les premiers seront les pionniers !\r\n\r\nPosted by <a href="https://www.facebook.com/unjourici06" target="_blank">Un jour ici, coworking à Sophia Antipolis</a> on <a href="https://www.facebook.com/unjourici06/posts/864121433669729:0" target="_blank">vendredi 3 juillet 2015</a></blockquote>\r\n</div>\r\n</div>', 'Notre nouvel espace de coworking à Sophia Antipolis', '', 'publish', 'closed', 'open', '', 'notre-nouvel-espace-de-coworking-a-sophia-antipolis', '', '', '2015-07-21 20:05:18', '2015-07-21 19:05:18', '', 0, 'http://mnemotix.com/?p=118', 0, 'post', '', 0),
(119, 3, '2015-07-08 11:18:56', '2015-07-08 10:18:56', 'Après plusieurs mois de travail et de co-construction avec le collectif de coWorking et coEvolution un jour ici... nous voici cet été installés à l''Ademe. Mnémotix se joint à plusieurs structures et entrepreneurs pour proposer l''animation d''un espace de coworking dont les concepts clés sont : gouvernance collaborative, intelligence collective, innovation transformative, économie de la fonctionnalité et de la coopération, développement durable, innovation sociale, modèles d''affaire alternatifs, culture numérique 3.0.\r\n\r\nVenez découvrir le Summer Camp un été ici à l''Ademe Sophia Antipolis\r\n<div class="fb-post" data-href="https://www.facebook.com/unjourici06/posts/864121433669729:0" data-width="500">\r\n<div class="fb-xfbml-parse-ignore">\r\n<blockquote cite="https://www.facebook.com/unjourici06/posts/864121433669729:0">Bonne nouvelle ! un jour ici... s''installe à l''ADEME Sophia Antipolis pour un été ici... incubation de son expérience de coWorking &amp; coEvolution ! Participez les premiers seront les pionniers !\r\n\r\nPosted by <a href="https://www.facebook.com/unjourici06">Un jour ici, coworking à Sophia Antipolis</a> on <a href="https://www.facebook.com/unjourici06/posts/864121433669729:0">vendredi 3 juillet 2015</a></blockquote>\r\n</div>\r\n</div>', 'Notre nouvel espace de coworking à Sophia Antipolis', '', 'inherit', 'closed', 'open', '', '118-revision-v1', '', '', '2015-07-08 11:18:56', '2015-07-08 10:18:56', '', 118, 'http://mnemotix.com/blogtemp/118-revision-v1/', 0, 'revision', '', 0),
(120, 3, '2015-07-08 11:35:46', '2015-07-08 10:35:46', 'Après plusieurs mois de travail et de co-construction avec le collectif de coWorking et coEvolution un jour ici... nous voici cet été installés à l''Ademe. Mnémotix se joint à plusieurs structures et entrepreneurs pour proposer l''animation d''un espace de coworking dont les concepts clés sont : gouvernance collaborative, intelligence collective, innovation transformative, économie de la fonctionnalité et de la coopération, développement durable, innovation sociale, modèles d''affaire alternatifs, culture numérique 3.0.\r\n\r\nVenez découvrir le Summer Camp un été ici à l''Ademe Sophia Antipolis\r\n<div class="fb-post" data-href="https://www.facebook.com/unjourici06/posts/864121433669729:0" data-width="500">\r\n<div class="fb-xfbml-parse-ignore">\r\n<blockquote cite="https://www.facebook.com/unjourici06/posts/864121433669729:0">Bonne nouvelle ! un jour ici... s''installe à l''ADEME Sophia Antipolis pour un été ici... incubation de son expérience de coWorking &amp; coEvolution ! Participez les premiers seront les pionniers !\r\n\r\nPosted by <a href="https://www.facebook.com/unjourici06" target="_blank">Un jour ici, coworking à Sophia Antipolis</a> on <a href="https://www.facebook.com/unjourici06/posts/864121433669729:0" target="_blank">vendredi 3 juillet 2015</a></blockquote>\r\n</div>\r\n</div>', 'Notre nouvel espace de coworking à Sophia Antipolis', '', 'inherit', 'closed', 'open', '', '118-revision-v1', '', '', '2015-07-08 11:35:46', '2015-07-08 10:35:46', '', 118, 'http://mnemotix.com/blogtemp/118-revision-v1/', 0, 'revision', '', 0),
(121, 3, '2015-07-08 13:32:53', '2015-07-08 12:32:53', 'Dans le cadre des <a href="http://devlog.cnrs.fr/jdev2015">Journées nationales du DEVeloppement logiciel de l''Enseignement Supérieur et Recherche (JDEVs)</a> à Bordeaux, Mnémotix est intervenu pour l''animation de l''atelier : <a href="http://devlog.cnrs.fr/jdev2015/t7.a08" target="_blank">T7.A08 Développement de Single Page Application avec BackboneJS + Marionette : exemple de connexion cross-domain à une API REST</a>. Vous trouverez les tutoriaux réalisés à cet effet ci-dessous :\r\n<ul>\r\n	<li><a href="http://mnemotix.github.io/backbone-tuto/" target="_blank">Apprendre BackboneJS</a> : Créer un client REST complet avec Gulp et Backbone</li>\r\n	<li><a href="http://mnemotix.github.io/gulp-tuto/" target="_blank">Apprendre GULP</a> : créer un projet Gulp avec browserify</li>\r\n</ul>\r\n<h2></h2>', 'Tutoriels Mnémotix', '', 'publish', 'closed', 'open', '', 'tutoriels-mnemotix', '', '', '2015-07-21 20:05:49', '2015-07-21 19:05:49', '', 0, 'http://mnemotix.com/?p=121', 0, 'post', '', 0),
(116, 3, '2015-06-08 10:55:36', '2015-06-08 09:55:36', 'Mnémotix a fait partie de cette première initiative d''un Mooc dédié à l''écotourisme, accessible via la plateforme FUN (Voir : <a href="https://www.france-universite-numerique-mooc.fr/courses/ujendouba/36001/session01/about" target="_blank">L''écotourisme : imaginons le ensemble</a>) et a développé un module sur "l''apport de l''économie collaborative et des nouvelles plateformes numériques pour favoriser l''écotourisme".\r\n\r\n<iframe src="//www.dailymotion.com/embed/video/x2h34zs" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n<a href="http://www.dailymotion.com/video/x2h34zs_fun-mooc-ecotourisme-imaginons-le-ensemble_school" target="_blank">FUN MOOC : Écotourisme, imaginons-le ensemble</a> <i>par <a href="http://www.dailymotion.com/fr-universite-numerique" target="_blank">fr-universite-numerique</a></i>', 'Notre participation au 1er Mooc sur l''écotourisme', '', 'publish', 'closed', 'open', '', 'notre-participation-au-1er-mooc-sur-lecotourisme', '', '', '2015-07-21 20:07:04', '2015-07-21 19:07:04', '', 0, 'http://mnemotix.com/?p=116', 0, 'post', '', 0),
(117, 3, '2015-07-08 11:07:39', '2015-07-08 10:07:39', 'Mnémotix a fait partie de cette première initiative d''un Mooc dédié à l''écotourisme, accessible via la plateforme FUN (Voir : <a href="https://www.france-universite-numerique-mooc.fr/courses/ujendouba/36001/session01/about" target="_blank">L''écotourisme : imaginons le ensemble</a>) et a développé un module sur "l''apport de l''économie collaborative et des nouvelles plateformes numériques pour favoriser l''écotourisme".\r\n<iframe src="//www.dailymotion.com/embed/video/x2h34zs" width="480" height="270" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\r\n<a href="http://www.dailymotion.com/video/x2h34zs_fun-mooc-ecotourisme-imaginons-le-ensemble_school" target="_blank">FUN MOOC : Écotourisme, imaginons-le ensemble</a> <i>par <a href="http://www.dailymotion.com/fr-universite-numerique" target="_blank">fr-universite-numerique</a></i>', 'Notre participation au 1er Mooc sur l''écotourisme', '', 'inherit', 'closed', 'open', '', '116-revision-v1', '', '', '2015-07-08 11:07:39', '2015-07-08 10:07:39', '', 116, 'http://mnemotix.com/blogtemp/116-revision-v1/', 0, 'revision', '', 0),
(101, 1, '2014-11-02 16:28:50', '2014-11-02 15:28:50', ' ', '', '', 'publish', 'closed', 'open', '', '101', '', '', '2014-11-02 16:28:50', '2014-11-02 15:28:50', '', 1, 'http://mnemotix.com/live/?p=101', 2, 'nav_menu_item', '', 0),
(102, 1, '2014-11-02 16:28:50', '2014-11-02 15:28:50', ' ', '', '', 'publish', 'closed', 'open', '', '102', '', '', '2014-11-02 16:28:50', '2014-11-02 15:28:50', '', 1, 'http://mnemotix.com/live/?p=102', 3, 'nav_menu_item', '', 0);

DROP TABLE IF EXISTS `mnx_terms`;
CREATE TABLE IF NOT EXISTS `mnx_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

INSERT INTO `mnx_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Blog', 'blog', 0),
(33, 'Technologie', 'techno', 0),
(4, 'Publications', 'publications', 0),
(5, 'Tutoriels', 'tutoriels', 0),
(6, 'Tag 1', 'tag-1', 0),
(7, 'Tag 2', 'tag-2', 0),
(8, 'Tag 3', 'tag-3', 0),
(9, 'Tag 4', 'tag-4', 0),
(10, 'Veille', 'veille', 0),
(11, 'Linked Open Data', 'linked-open-data', 0),
(12, 'Open Data', 'open-data', 0),
(13, 'Web sémantique', 'web-semantique', 0),
(14, 'post-format-aside', 'post-format-aside', 0),
(15, 'post-format-video', 'post-format-video', 0),
(16, 'post-format-quote', 'post-format-quote', 0),
(17, 'side', 'side', 0),
(18, 'blog_categories', 'blog_categories', 0),
(19, 'mooc', 'mooc', 0),
(20, 'écotourisme', 'ecotourisme', 0),
(21, 'économie collaborative', 'economie-collaborative', 0),
(22, 'coworking', 'coworking', 0),
(23, 'Ademe', 'ademe', 0),
(24, 'un jour ici', 'un-jour-ici', 0),
(25, 'tutoriel', 'tutoriel', 0),
(26, 'web sémantique', 'web-semantique', 0),
(27, 'Jdevs', 'jdevs', 0),
(28, 'Gulp', 'gulp', 0),
(29, 'BackboneJS', 'backbonejs', 0),
(30, 'Sophia Antipolis', 'sophia-antipolis', 0),
(31, 'ESS', 'ess', 0),
(32, 'Co-Working', 'co-working', 0),
(34, 'Formation', 'formation', 0),
(35, 'MOOC', 'mooc', 0),
(36, 'Javascript', 'javascript', 0);

DROP TABLE IF EXISTS `mnx_term_relationships`;
CREATE TABLE IF NOT EXISTS `mnx_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `mnx_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(118, 32, 0),
(118, 30, 0),
(102, 18, 0),
(118, 23, 0),
(118, 22, 0),
(116, 35, 0),
(116, 21, 0),
(116, 20, 0),
(116, 19, 0),
(101, 18, 0),
(118, 24, 0),
(121, 33, 0),
(116, 34, 0),
(121, 5, 0),
(121, 25, 0),
(121, 26, 0),
(121, 27, 0),
(121, 28, 0),
(121, 29, 0);

DROP TABLE IF EXISTS `mnx_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `mnx_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

INSERT INTO `mnx_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', 'Cette section est dédiée à l''actualité ainsi qu''aux publications Mnémotix', 0, 0),
(33, 33, 'category', 'Actualité technologique', 1, 1),
(4, 4, 'category', '', 1, 0),
(5, 5, 'category', '', 1, 1),
(6, 6, 'post_tag', 'description du tag 1.', 0, 0),
(7, 7, 'post_tag', 'description du tag 2.', 0, 0),
(8, 8, 'post_tag', 'description du tag 3.', 0, 0),
(9, 9, 'post_tag', 'description du tag 4.', 0, 0),
(10, 10, 'category', '', 1, 0),
(34, 34, 'category', '', 1, 1),
(11, 11, 'category', '', 33, 0),
(12, 12, 'category', '', 33, 0),
(13, 13, 'category', 'Ressources consacrées aux technologies du Web sémantique et du Web de données.', 33, 0),
(14, 14, 'post_format', '', 0, 0),
(15, 15, 'post_format', '', 0, 0),
(16, 16, 'post_format', '', 0, 0),
(17, 17, 'nav_menu', '', 0, 0),
(18, 18, 'nav_menu', '', 0, 2),
(30, 30, 'category', '', 1, 1),
(31, 31, 'category', '', 1, 0),
(32, 32, 'category', '', 1, 1),
(35, 35, 'category', '', 34, 1),
(36, 36, 'category', '', 33, 0),
(19, 19, 'post_tag', '', 0, 1),
(20, 20, 'post_tag', '', 0, 1),
(21, 21, 'post_tag', '', 0, 1),
(22, 22, 'post_tag', '', 0, 1),
(23, 23, 'post_tag', '', 0, 1),
(24, 24, 'post_tag', '', 0, 1),
(25, 25, 'post_tag', '', 0, 1),
(26, 26, 'post_tag', '', 0, 1),
(27, 27, 'post_tag', '', 0, 1),
(28, 28, 'post_tag', '', 0, 1),
(29, 29, 'post_tag', '', 0, 1);

DROP TABLE IF EXISTS `mnx_usermeta`;
CREATE TABLE IF NOT EXISTS `mnx_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;

INSERT INTO `mnx_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'mnx_capabilities', 'a:2:{s:13:"administrator";b:1;s:14:"backwpup_admin";b:1;}'),
(11, 1, 'mnx_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets,wp410_dfw'),
(13, 1, 'show_welcome_panel', '0'),
(14, 1, 'session_tokens', 'a:7:{s:64:"d14eab33a0b616a09313bf5a56c81b70f73db413423e169f1f88cb426cfe6678";a:4:{s:10:"expiration";i:1439286685;s:2:"ip";s:14:"78.250.142.192";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36";s:5:"login";i:1439113885;}s:64:"5118b4991253867f23f6b1d3e9e149c7d5d96f275e6457e4a92316cbc1e13682";a:4:{s:10:"expiration";i:1439114300;s:2:"ip";s:14:"78.250.142.192";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36";s:5:"login";i:1439114000;}s:64:"124792c6fcac18cb1aca053f26bdfc4cfd4cab621ca1c177212366a01731d945";a:4:{s:10:"expiration";i:1439114300;s:2:"ip";s:14:"78.250.142.192";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36";s:5:"login";i:1439114000;}s:64:"ee0c9c0f6ff9e7946601061bf0f01ce1d95591ebdb189c9a66155f2b4988ec93";a:4:{s:10:"expiration";i:1439114328;s:2:"ip";s:14:"78.250.142.192";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36";s:5:"login";i:1439114028;}s:64:"78de8f02632d529cea5a3ce22bf78f94b79932bf87fe5b7b136652f386070e6d";a:4:{s:10:"expiration";i:1439114328;s:2:"ip";s:14:"78.250.142.192";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36";s:5:"login";i:1439114028;}s:64:"a0ed36fe493e32585aad444471cddcca5a7626b2af414f06d39a3f6446453256";a:4:{s:10:"expiration";i:1439114361;s:2:"ip";s:14:"78.250.142.192";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36";s:5:"login";i:1439114061;}s:64:"296952220a71fa81633db2530bcac6ddb6fcb6cf04aeba48a50aba42cb4f38a5";a:4:{s:10:"expiration";i:1439114361;s:2:"ip";s:14:"78.250.142.192";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36";s:5:"login";i:1439114061;}}'),
(15, 1, 'mnx_dashboard_quick_press_last_post_id', '128'),
(16, 1, 'closedpostboxes_dashboard', 'a:1:{i:0;s:17:"dashboard_primary";}'),
(17, 1, 'metaboxhidden_dashboard', 'a:2:{i:0;s:21:"dashboard_quick_press";i:1;s:17:"dashboard_primary";}'),
(18, 1, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:19:"dashboard_right_now";s:4:"side";s:58:"dashboard_quick_press,dashboard_activity,dashboard_primary";s:7:"column3";s:0:"";s:7:"column4";s:0:"";}'),
(19, 1, 'closedpostboxes_post', 'a:2:{i:0;s:9:"formatdiv";i:1;s:10:"wpseo_meta";}'),
(20, 1, 'metaboxhidden_post', 'a:9:{i:0;s:13:"postvideo-box";i:1;s:11:"postexcerpt";i:2;s:13:"trackbacksdiv";i:3;s:10:"postcustom";i:4;s:16:"commentstatusdiv";i:5;s:7:"slugdiv";i:6;s:9:"authordiv";i:7;s:12:"revisionsdiv";i:8;s:11:"commentsdiv";}'),
(21, 1, 'meta-box-order_post', 'a:3:{s:4:"side";s:75:"postvideo-box,postimagediv,submitdiv,formatdiv,categorydiv,tagsdiv-post_tag";s:6:"normal";s:71:"postexcerpt,trackbacksdiv,postcustom,commentstatusdiv,slugdiv,authordiv";s:8:"advanced";s:0:"";}'),
(22, 1, 'screen_layout_post', '2'),
(23, 1, 'mnx_user-settings', 'editor_expand=on&libraryContent=browse&hidetb=1&editor=html&mfold=o'),
(24, 1, 'mnx_user-settings-time', '1432131017'),
(25, 2, 'nickname', 'nico'),
(26, 2, 'first_name', 'Nicolas'),
(27, 2, 'last_name', 'Delaforge'),
(28, 2, 'description', ''),
(29, 2, 'rich_editing', 'true'),
(30, 2, 'comment_shortcuts', 'false'),
(31, 2, 'admin_color', 'fresh'),
(32, 2, 'use_ssl', '0'),
(33, 2, 'show_admin_bar_front', 'true'),
(34, 2, 'mnx_capabilities', 'a:2:{s:13:"administrator";b:1;s:14:"backwpup_admin";b:1;}'),
(35, 2, 'mnx_user_level', '10'),
(36, 2, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets,wp410_dfw'),
(38, 2, 'mnx_dashboard_quick_press_last_post_id', '125'),
(39, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(40, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:8:"add-post";i:1;s:12:"add-post_tag";i:2;s:15:"add-post_format";}'),
(41, 1, 'closedpostboxes_page', 'a:0:{}'),
(42, 1, 'metaboxhidden_page', 'a:6:{i:0;s:12:"revisionsdiv";i:1;s:10:"postcustom";i:2;s:16:"commentstatusdiv";i:3;s:11:"commentsdiv";i:4;s:7:"slugdiv";i:5;s:9:"authordiv";}'),
(43, 1, 'mnx_yarpp_saw_optin', '1'),
(44, 1, 'nav_menu_recently_edited', '18'),
(45, 1, 'manageedit-postcolumnshidden', 'a:12:{i:0;s:4:"tags";i:1;s:8:"comments";i:2;s:11:"wpseo-score";i:3;s:11:"wpseo-title";i:4;s:14:"wpseo-metadesc";i:5;s:13:"wpseo-focuskw";i:6;s:0:"";i:7;s:0:"";i:8;s:0:"";i:9;s:0:"";i:10;s:0:"";i:11;s:0:"";}'),
(46, 3, 'nickname', 'mylene'),
(47, 3, 'first_name', 'Mylène'),
(48, 3, 'last_name', 'Leitzelman'),
(49, 3, 'description', ''),
(50, 3, 'rich_editing', 'true'),
(51, 3, 'comment_shortcuts', 'false'),
(52, 3, 'admin_color', 'ocean'),
(53, 3, 'use_ssl', '0'),
(54, 3, 'show_admin_bar_front', 'true'),
(55, 3, 'mnx_capabilities', 'a:2:{s:13:"administrator";b:1;s:14:"backwpup_admin";b:1;}'),
(56, 3, 'mnx_user_level', '10'),
(57, 3, 'dismissed_wp_pointers', 'wp360_locks,wp390_widgets,wp410_dfw'),
(58, 3, 'session_tokens', 'a:2:{s:64:"3fa2807a95027c4b79c484f23cbadd866e9456702ddbfc2c2f14e7764722c303";a:4:{s:10:"expiration";i:1437141137;s:2:"ip";s:12:"78.237.21.48";s:2:"ua";s:102:"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36";s:5:"login";i:1436968337;}s:64:"a45312b3444e2ab30fefb7be53189cb10d3501f5fb7454205a637b040b72b7c7";a:4:{s:10:"expiration";i:1437141263;s:2:"ip";s:12:"78.237.21.48";s:2:"ua";s:102:"Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36";s:5:"login";i:1436968463;}}'),
(59, 3, 'mnx_dashboard_quick_press_last_post_id', '115'),
(60, 3, 'closedpostboxes_dashboard', 'a:0:{}'),
(61, 3, 'metaboxhidden_dashboard', 'a:2:{i:0;s:21:"dashboard_quick_press";i:1;s:17:"dashboard_primary";}'),
(62, 3, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:19:"dashboard_right_now";s:4:"side";s:58:"dashboard_quick_press,dashboard_primary,dashboard_activity";s:7:"column3";s:0:"";s:7:"column4";s:0:"";}'),
(63, 3, 'manageedit-postcolumnshidden', 'a:2:{i:0;s:8:"comments";i:1;s:0:"";}'),
(64, 3, 'closedpostboxes_post', 'a:0:{}'),
(65, 3, 'metaboxhidden_post', 'a:11:{i:0;s:13:"postvideo-box";i:1;s:9:"formatdiv";i:2;s:12:"revisionsdiv";i:3;s:11:"postexcerpt";i:4;s:13:"trackbacksdiv";i:5;s:10:"postcustom";i:6;s:16:"commentstatusdiv";i:7;s:11:"commentsdiv";i:8;s:7:"slugdiv";i:9;s:9:"authordiv";i:10;s:18:"yarpp_relatedposts";}'),
(66, 3, 'meta-box-order_post', 'a:3:{s:4:"side";s:75:"postvideo-box,submitdiv,formatdiv,categorydiv,tagsdiv-post_tag,postimagediv";s:6:"normal";s:115:"revisionsdiv,postexcerpt,trackbacksdiv,postcustom,commentstatusdiv,commentsdiv,slugdiv,authordiv,yarpp_relatedposts";s:8:"advanced";s:0:"";}'),
(67, 3, 'screen_layout_post', '2'),
(71, 3, 'mnx_user-settings', 'editor=tinymce'),
(72, 3, 'mnx_user-settings-time', '1436351748'),
(74, 2, 'session_tokens', 'a:1:{s:64:"c6bfa76f96c9b862165ddf85ccb631cd4b63df35fb4cb598f440d8cb760cf583";a:4:{s:10:"expiration";i:1437671694;s:2:"ip";s:14:"86.219.230.119";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36";s:5:"login";i:1437498894;}}'),
(68, 2, 'mnx_user-settings', 'posts_list_mode=excerpt&mfold=o'),
(69, 2, 'mnx_user-settings-time', '1434705904'),
(75, 2, 'nav_menu_recently_edited', '18'),
(76, 2, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(77, 2, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:8:"add-post";i:1;s:12:"add-post_tag";i:2;s:15:"add-post_format";}');

DROP TABLE IF EXISTS `mnx_users`;
CREATE TABLE IF NOT EXISTS `mnx_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `mnx_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BeYa5awmmm.9qTuXZuPQXyiwuN5sEB1', 'admin', 'pymichel@gmail.com', '', '2014-10-20 13:46:59', '$P$ByzgVGipNYxiIfYBQ5Kl8nAUwxcq6a1', 0, 'admin'),
(2, 'nico', '$P$BgusRqz0OAEZ65vZW1AU8ud1ZRCMyl0', 'nico', 'nicolas.delaforge@mnemotix.com', '', '2014-10-21 09:47:03', '', 0, 'Nicolas Delaforge'),
(3, 'mylene', '$P$BvWvjCc1zxZDGxzZpL75fEf.WUkbfS.', 'mylene', 'mylene.leitzelman@mnemotix.com', '', '2015-05-20 15:37:41', '$P$B6N4WSqZJwB7uY68xAhBE.A/aQVS3N/', 0, 'Mylène Leitzelman');

DROP TABLE IF EXISTS `mnx_yarpp_related_cache`;
CREATE TABLE IF NOT EXISTS `mnx_yarpp_related_cache` (
  `reference_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `score` float unsigned NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `mnx_yarpp_related_cache` (`reference_ID`, `ID`, `score`, `date`) VALUES
(118, 0, 0, '2015-07-21 19:05:20'),
(116, 0, 0, '2015-07-21 19:07:07'),
(121, 0, 0, '2015-07-21 19:05:52');


ALTER TABLE `mnx_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

ALTER TABLE `mnx_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

ALTER TABLE `mnx_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

ALTER TABLE `mnx_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

ALTER TABLE `mnx_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

ALTER TABLE `mnx_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD FULLTEXT KEY `yarpp_title` (`post_title`);
ALTER TABLE `mnx_posts`
  ADD FULLTEXT KEY `yarpp_content` (`post_content`);

ALTER TABLE `mnx_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `name` (`name`(191)),
  ADD KEY `slug` (`slug`(191));

ALTER TABLE `mnx_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

ALTER TABLE `mnx_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

ALTER TABLE `mnx_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

ALTER TABLE `mnx_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`);

ALTER TABLE `mnx_yarpp_related_cache`
  ADD PRIMARY KEY (`reference_ID`,`ID`),
  ADD KEY `score` (`score`),
  ADD KEY `ID` (`ID`);


ALTER TABLE `mnx_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `mnx_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `mnx_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `mnx_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10744;
ALTER TABLE `mnx_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=421;
ALTER TABLE `mnx_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=129;
ALTER TABLE `mnx_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
ALTER TABLE `mnx_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
ALTER TABLE `mnx_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
ALTER TABLE `mnx_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

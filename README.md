## Site Web Mnemotix

## Dev

Install "php"

Run

```
 cd www/ && php -S localhost:3000
```

http://localhost:3000/

## Build a version

```
  git add .
  git commit -m "message"
  git tag -a "1.0.9" -m  "v1.0.9"  ( derniere version )
  git push origin [TAG NAME]     ou       git push origin master --tags
```

Gitlab CI will run a pipeline to build a docker image on https://gitlab.com/mnemotix/mnemotix-website/container_registry

## Deploy a version

Go to Rancher :

Stack : "Mnemotix"
Container : "website"

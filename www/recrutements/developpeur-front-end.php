<!DOCTYPE html>

<html lang="fr">
<head>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta name="description" content="Donnez du sens à vos données.">
  <meta name="author" content="Mnemotix">

  <title>Nous cherchons un·e développeu·r·se Web (Fullstack Javascript)</title>

  <!-- Links -->
  <?php include '../includes/links.php'; ?>
</head>

<body class="home">

<!-- ============================================================= HEADER ============================================================= -->

<header class="headroom">
  <?php include '../includes/navbar.php'; ?>
</header>


<!-- ============================================================== MAIN ============================================================== -->

<main>
  <div class="container inner">
    <div class="hiring-pre-title">
      NOUS RECHERCHONS
    </div>

    <h1 class="text-center">
      Un·e développeu·r·se Web (Fullstack Javascript)
    </h1>

    <p></p>

    <h2>
      Le poste
    </h2>

    <p>
      Intégré à l'équipe technique et appuyé sur la méthodologie Agile, chaque
      développeur prend part au développement des différentes applications
      branchées sur notre chaîne de traitement sémantique. Pour ces
      applications, nous avons fait le choix d'être fullstack Javascript. Aussi
      chez nous, il faut s'attendre à apprécier particulièrement :
    </p>

    <ul class="circled">
      <li>
        Le javascript serveur sur Node.js. Avec notamment Express.js et Webpack.js.
      </li>
      <li>
        Le javascript client avec notamment React (et/ou React Native), Apollo client et leur format d’échange GraphQL.

      </li>
      <li>
        Les bases de données RDF (notamment GraphDB) et le langage de requête SPARQL.
      </li>
    </ul>

    <p>
      Nous sommes conscients que la partie base de données sort des architectures traditionnelles et peut décontenancer. Pourtant nous sommes convaincus de la pertinence de cette technologie tant par sa simplicité que par sa puissance. Nous sommes aussi conscients que cette technologie reste marginale. C’est pourquoi nous n’attendons pas du candidat qu’il la maîtrise d’entrée, juste qu’il soit assez curieux pour sortir des sentiers battus.
    </p>

    <p>
      Dans la mesure où rien est figé dans le marbre et que les technologies
      sont en perpétuelle évolution, nous affectionnons particulièrement les
      personnes curieuses, ouvertes, autodidactes et rapidement autonomes.
    </p>

    <p>
      Le périmètre de fonctions qu'un développeur peut avoir chez nous est
      large:
    </p>

    <p>
    <ul class="circled">
      <li>
        Participation aux propositions techniques lors de la conception
        d'applications.
      </li>
      <li>
        Planification de ses tâches dans des sprint plannings Agile.
      </li>
      <li>
        Maquettage et prototypage pour un client en panne d'inspiration.
      </li>
      <li>
        Idéalement un peu de graphisme (même si on a bien conscience que c'est un
        métier).
      </li>
      <li>
        Développement (et oui un peu quand même).
      </li>
      <li>
        Débuggage (et oui aussi…)
      </li>
      <li>
        Réalisation de tests unitaires et d'intégration continue.
      </li>
      <li>
        Maintenance et suivi des applications hébergées et exploitées en
        production.
      </li>
    </ul>
    </p>

    <h2 class="top">
      Pourquoi venir chez nous ?
    </h2>

    <p>
      Nous sommes partis d’un projet coopératif et nous continuerons sur un mode coopératif. Notre politique salariale est donc totalement transparente. Si en plus l'idée d'être actionnaire de votre propre entreprise vous branche
      et que vous nourrissez le doux rêve d'un monde où les
      salariés prennent en main leur destin, vous êtes au bon endroit.
    </p>

    <p>
      Nous sommes une coopérative en croissance, il reste plein de choses à
      construire et donc plein de choses à s'approprier. Si vous avez l'âme
      aventurière, vous êtes au bon endroit.
    </p>

    <p>
      Nous sommes situé à l'ouest de la Côte-d'Azur donc avec pas trop de
      bouchons...
    </p>

    <h2>
      Interessé.e ?
    </h2>

    <p>
      Envoyez nous votre CV, lettre de motivation et tout ce qui vous fera plaisir <a href="/#contact">ici</a>.
    </p>
  </div>
</main>

<!-- ============================================================= FOOTER ============================================================= -->

<footer class="dark-bg">
  <?php include '../includes/footer.php'; ?>
</footer>


<!-- =============================================================== JS =============================================================== -->

<!-- Scripts -->
<?php include '../includes/scripts.php'; ?>

</body>
</html>

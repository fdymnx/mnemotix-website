<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="assets/js/jquery.easing.1.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="assets/js/skrollr.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/js/waypoints-sticky.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script> 

<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));</script>

<script src="assets/js/scripts.js"></script>

<!-- HEADER EFFECTS -->
<script src="assets/js/headroom.min.js"></script>
<script>
	var myElement = document.querySelector("header");
	var headroom = new Headroom(myElement, {
		"offset": 90,
		"tolerance": 0
	});
	headroom.init();

	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			console.log(this.pathname);
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top - 50
					}, 1000);
					return false;
				}
			}
		});
	});
</script>

<!-- FORM VALIDATION -->

<script src="assets/js/jquery.form.js"></script>
<script src="assets/js/jquery.validate.min.js"></script>
<script>
	$(document).ready(function() {

		$('#contactform, #commentform').validate({
			focusInvalid: false,

			rules: {
				name: {
					required: true,
					minlength: 2
				},
				email: {
					required: true,
					email: true
				},
				message: {
					required: true,
					minlength: 10
				}
			},

			messages: {
				name: {
					required: 'Veuillez entrer votre nom.',
					minlength: jQuery.format('Veuillez entrer un nom valide.')
				},
				email: {
					required: 'Veuillez entrer votre email.',
					email: 'Veuillez entrer un email valide.'
				},
				message: {
					required: 'Veuillez entrer un message.',
					minlength: jQuery.format('Veuillez entrer un message valide (au moins 10 caractères).')
				}
			},

			submitHandler: function(form) {
				$('#contactform .btn-submit').html('Envoi en cours ...');

				$(form).ajaxSubmit({
					success: function(responseText, statusText, xhr, $form) {
						$(form).delay(1300).slideUp('fast');
						$('#response').html(responseText).hide().delay(1300).slideDown('fast');
					}
				});
				return false;
			}

		});

	});
</script>
<div class="navbar ">

    <div class="navbar-header">
        <div class="container">
            <!-- LOGO MOBILE ============================================================= -->
            <a class="navbar-brand" href="/">
                <img src="/assets/images/logo.png" class="logo" alt="logo Mnemotix" srcset="/assets/images/logo.svg"/>
            </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
    </div>

    <div class="yamm">
        <div class="navbar-collapse collapse">
            <div class="container">

                <!-- LOGO ======================================================================== -->

                <a class="navbar-brand" href="/">                 
                    <img src="/assets/images/logo.png" class="logo" alt="logo Mnemotix" srcset="/assets/images/logo.svg"/>
                </a>

                <!-- MAIN NAVIGATION ============================================================= -->

                <ul class="nav navbar-nav">

                    <li class="services-menu">
                        <a href="/#methodologie">Méthodologie</a>
                    </li>

                    <li class="services-menu">
                        <a href="/#services">Services</a>
                    </li>


                    <li class="about-menu">
                        <a href="/#about">Mnemotix</a>
                    </li>

                    <li class="clients-menu">
                        <a href="/#testimonials">Clients</a>
                    </li>

                    <li class="actus-menu">
                        <a href="/#actus">Actualités</a>
                    </li>

                    <li class="actus-menu">
                        <a href="/#recrutement">Recrutement</a>
                    </li>

                    <li class="contact-menu">
                        <a href="/#contact">Contact</a>
                    </li>

                    <li class="twitter-menu">
                        <!-- <a href="https://twitter.com/mnemotix" target="blank" class="visible-xs-block visible-sm-block">See us on Twitter</a> -->
                        <a href="https://twitter.com/mnemotix" target="blank" title="See us on Twitter" class="visible-md-block visible-lg-block"><i class="icon-twitter"></i></a>
                    </li>

                </ul>

            </div>
        </div>
    </div>

</div>
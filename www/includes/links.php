       
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Customizable CSS -->
<link href="/assets/css/main.css" rel="stylesheet">

<link href="/assets/fonts/fontello/fontello.css" rel="stylesheet">

<link href="https://fonts.googleapis.com/css2?family=Rubik&display=swap" rel="stylesheet">

<!-- PROD CSS -->
<link href="/assets/css/prod.min.css" rel="stylesheet">

<!-- Favicon -->
<link rel="shortcut icon" href="/assets/images/favicon.png">

<!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
<!--[if lt IE 9]>
    <script src="/assets/js/html5shiv.js"></script>
    <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<div class="footer-bottom">
	<div class="container inner">
		<p class="pull-left">© 2020 mnemotix, tous droits réservés.</p>
		<ul class="footer-menu pull-right">
			<li><a href="/">Accueil</a></li>
			<li><a href="#services">Services</a></li>
			<li><a href="#actus">Actualités</a></li>
			<li><a href="#clients">Clients</a></li>
			<li><a href="#about">A propos</a></li>
			<li><a href="#contact">Contact</a></li>
		</ul>
	</div>
</div>
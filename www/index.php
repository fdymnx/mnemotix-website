  <!DOCTYPE html>

  <html lang="fr">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Donnez du sens à vos données.">
    <meta name="author" content="Mnemotix">
    <title>Donnez du sens à vos données.</title>
    <?php include 'includes/links.php'; ?>
  </head>

  <body class="home">
    <header class="headroom">
      <?php include 'includes/navbar.php'; ?>
    </header>

    <main>
      <section id="hero">
        <div id="owl-main" class="owl-carousel height-md owl-inner-nav owl-ui-lg">

          <div class="item slide1">
            <div class="container">
              <div class="caption vertical-center text-left">
                <div class="light-color">
                  Nous structurons et relions vos données pour les rendre intelligentes quels que soient leur volume et leur complexité
                </div>
              </div>
            </div>
          </div>

          <div class="item slide2">
            <div class="container">
              <div class="caption vertical-center text-left">
                <div class="light-color">
                  Optez pour du sur-mesure
                  <p class="fadeInRight-2">Nos solutions
                    personnalisées sont adaptées à la complexité de vos besoins
                  </p>
                  <a href="#services" class="btn btn-large">Voir nos services</a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>

      <section id="nos-outils">
        <div class="container inner">
          <div class="row">
            <div class="center-block">
              <h1 class="margin-15 uppercase text-center">
                <div class="orange-background-color inline-block">
                  Grâce au knowledge graph
                </div>
              </h1>
            </div>
          </div>

          <div class="row margin-top-45">
            <div class="center-block text-center">
              <img src="assets/images/art/croix.png" class="img-50">
              <h2 class="margin-15">
                Renforcez l'intelligence collective de votre organisation
              </h2>
            </div>
          </div>

          <div class="row margin-top-45">
            <div class="center-block text-center">
              <img src="assets/images/art/carre.png" class="img-50">
              <h2 class="margin-15">
                Mesurez l'impact de votre activité
              </h2>
            </div>
          </div>
          <div class="row margin-top-45">
            <div class="center-block text-center">
              <img src="assets/images/art/triangle.png" class="img-50">
              <h2 class="margin-15">
                Mobilisez les bonnes ressources au bon moment </h2>
            </div>
          </div>
        </div>
      </section>

      <section id="methodologie" class="blue-background-image">
        <div class="container inner-top inner-bottom-sm">
          <div class="row">
            <div class="col-md-8 col-sm-9 center-block text-center ">
              <h1 class="text-white">Notre méthodologie</h1>
            </div>
          </div>
        </div>
      </section>

      <section id="product">
        <div class="container inner">

          <div class="row">
            <div class="col-lg-5 col-sm-6 inner-right-xs inner-bottom-xs text-right ">
              <figure><img src="assets/images/illustrations/01.png" alt=""></figure>            
            </div>
            <div class="col-lg-7 col-sm-6 inner-left-xs ">
              <h2>Vos données sont dispersées</h2>
              <p class="text-medium text-justify">
                Elles sont sur différents supports, dans différents formats et vous ne pouvez pas les exploiter.
              </p>
            </div>
          </div>

          <div class="row inner-top-md">
            <div class="col-lg-5 col-sm-6 inner-right-xs inner-bottom-xs text-right ">
              <figure><img src="assets/images/illustrations/03.png" alt=""></figure>
            </div>
            <div class="col-lg-7 col-sm-6 inner-left-xs ">
              <h2>Nous vous aidons à les structurer, les décloisonner et les lier</h2>
              <p class="text-medium text-justify">
                Quels que soient leur volume et leur complexité, nous tissons des liens entres les données pour qu'elles soient beaucoup plus utilisables.
              </p>
            </div>
          </div>

          <div class="row inner-top-md">

            <div class="col-lg-5 col-sm-6 inner-right-xs inner-bottom-xs text-right ">
              <figure><img src="assets/images/illustrations/04.png" alt=""></figure>
              <!-- <figure class="hidden-xs"><?php include 'assets/images/illustrations/04.php'; ?></figure> -->
            </div>


            <div class="col-lg-7 col-sm-6 inner-left-xs ">
              <h2>Vos données ont un sens et sont utiles</h2>
              <p class="text-medium text-justify">
                Nous vous proposons des solutions à forte valeur-ajoutée: cartographie, recommandation, calcul de métrique, dashboard, etc.
              </p>
            </div>
          </div>
        </div>
      </section>



      <section id="services" class="orange-background-image">
        <div class="container inner-top inner-bottom-sm">
          <div class="row">
            <div class="col-md-8 col-sm-9 center-block text-center ">
              <h1 class="text-white">Nos services</h1>
            </div>
          </div>
        </div>
      </section>




      <section id="services-details">
        <div class="container inner-top inner-bottom-sm">

          <div class="row text-center">
            <div class="col-sm-6 inner-right-xs inner-bottom-xs">
              <img src="assets/images/art/code.png" class="img-100">
              <h2 class="margin-15">Développement</h2>
              <p class="text-medium text-justify">
                Nous construisons des solutions innovantes, personnalisées et respectueuses des standards du web. Nous intervenons à tous les niveaux, du stockage de la données à sa visualisation et traitons de grand volumes.
              </p>
            </div>

            <div class="col-sm-6 inner-left-xs inner-bottom-xs">
              <img src="assets/images/art/server.png" class="img-100">
              <h2 class="margin-15">Hébergement</h2>
              <p class="text-medium text-justify">
                Notre infrastructure de production, à la fois souple et performante, est capable de supporter le déploiement de tous nos développements en garantissant des niveaux de performance et de sécurité élevés.
              </p>
            </div>
          </div>

          <div class="row text-center">
            <div class="col-sm-6 inner-right-xs inner-bottom-xs">
              <img src="assets/images/art/light.png" class="img-100">
              <h2 class="margin-15">Consulting</h2>
              <p class="text-medium text-justify">
                Nous vous conseillons et auditons vos systèmes d'information. Nous pouvons aussi vous accompagner dans la conception de nouvelles architectures logicielles et dans la mise à niveau des architectures existantes (Assistance à maitrise d'ouvrage).
              </p>
            </div>

            <div class="col-sm-6 inner-left-xs inner-bottom-xs">
              <img src="assets/images/art/three.png" class="img-100">
              <h2 class="margin-15">Modélisation</h2>
              <p class="text-medium text-justify">
                Première étape sur le chemin de l'Open Data et du Web des données, nous vous aidons à structurer les concepts centraux de l'entreprise. Nous vous accompagnons grâce à des outils éprouvés.
              </p>
            </div>
          </div>

          <!-- <div class="row text-center">
            <div class="col-sm-6  inner-bottom-xs"> -->
          <div class="center-div text-center">
            <div class="col-sm-6 margin-auto inner-bottom-xs"> 
              <img src="assets/images/art/formation.png" class="img-100">
              <h2 class="margin-15">Formation</h2>
              <p class="text-medium text-justify">
              Forte de son expertise à la fois scientifique et technologique en Ingénierie des Connaissances, Mnémotix développe depuis ses débuts une offre de formations couvrant un large panel de niveaux, de l'introduction à la modélisation des connaissances, jusqu'à l'usage avancé des technologies Big Data (GraphQL, ElasticSearch, etc.) et du Web Sémantique (RDF, SPARQL, etc.). Consultez <a href="https://gitlab.com/mnemotix/formations/-/raw/master/catalogue/Book-Formations-Mn%C3%A9motix-2020.pdf?inline=false" target="_blank">notre catalogue</a>, et n'hésitez pas à <a href="#contact">nous contacter</a> pour une formation personnalisée.</p>
            </div>            
          </div>



        </div>
      </section>


      <section id="about" class="blue-background-image">
        <div class="container inner-top inner-bottom-sm">
          <div class="row">
            <div class="col-md-8 col-sm-9 center-block text-center ">
              <h1 class="text-white">Mnemotix</h1>
            </div>
          </div>
        </div>
      </section>

      <section id="who-we-are">
        <div class="container inner-top inner-bottom-sm">
          <div class="row text-center">

            <div class="col-sm-6 inner-right-xs inner-bottom-xs ">
              <h2 class="margin-15">Une smart-up coopérative</h2>
              <p class="text-medium text-justify">
                Nous sommes une coopérative (une société d'intérêt collectif - SCIC), un modèle d'innovation sociale,
                notre gouvernance est transparente et regroupe salariés-associés, des clients bénéficaires, des experts et des partenaires divers.
              </p>
            </div>

            <div class="col-sm-6 inner-left-xs inner-bottom-xs ">
              <h2 class="margin-15">Une équipe issue de la recherche</h2>
              <p class="text-medium text-justify">
                Notre équipe interdisciplinaire est majoritairement issue de la recherche française (UTC, INRIA, CNRS, Telecom ParisTech) et continue à y entretenir des relations étroites. Ce lien privilégié nous permet notamment de participer à des projets de recherche nationaux ou européens, et de faire bénéficier à nos clients d’une technologie toujours à la pointe dans le domaine du web et des données.
              </p>
            </div>
          </div>

          <div class="row text-center">
            <div class="col-sm-6 inner-right-xs inner-bottom-xs ">
              <h2 class="margin-15">Des technologies high tech et open source</h2>
              <p class="text-medium text-justify">
                Nous proposons des solutions basées sur des technologies matures ques vos équipes techniques pourront prendre en main avec notre aide.<br>
                La dimension open source rend la solution durable et vous permettra de devenir autonomes.
              </p>
            </div>

            <div class="col-sm-6 inner-left-xs inner-bottom-xs ">
              <h2 class="margin-15">La création de communs logiciels</h2>
              <p class="text-medium text-justify">
                Vous pouvez mutualiser vos solutions pour obtenir des outils performants et durables, tout en partageant les coûts.
              </p>
            </div>
          </div>
        </div>
      </section>

      <section id="motivations" class="light-bg">
        <div class="container inner-top inner-bottom-sm">

          <div class="row">
            <div class="center-block text-center">
              <header>
                <h1>Nos valeurs</h1>
                <p class="text-medium text-justify">
                  Nous croyons au fort potentiel du numérique et du Web en
                  particulier, pour construire
                  la société de demain où les savoirs seront décloisonnés et
                  uniformément partagés.
                </p>
              </header>
            </div>
          </div>


          <div class="row outer-top-sm text-center">
            <div class="col-sm-4 outer-bottom-xs text-center">
              <i class="margin-bottom-20 text-center icon icon-heart-empty icn lg"></i>
              <h2 class="margin-bottom-20 text-center">Esprit coopératif</h2>
              <p class="text-medium text-justify">
                Nous croyons à la synergie des compétences et au fonctionnement en réseau plutôt qu'à la centralisation du savoir-faire.<br>
                Notre ambition est de fédérer un ensemble d'acteurs du numérique qui partagent notre vision.<br>
                Notre statut de <a href="http://www.economie.gouv.fr/cedef/economie-sociale-et-solidaire" target="_blank">SCIC</a> aime à aller dans ce sens.
              </p>
            </div>

            <div class="col-sm-4 outer-bottom-xs text-center">
              <i class="margin-bottom-20 text-center icon icon-cog-alt icn lg"></i>
              <h2 class="margin-bottom-20 text-center">Transfert d'innovation</h2>
              <p class="text-medium text-justify">
                Venant du monde de la recherche, nous préservons un lien étroit avec de prestigieux laboratoires.
                <br>
                Nos développements intègrent des briques logicielles issues de la recherche que nous participons activement à rendre plus stables.
                <br>
                Cette approche permet à nos clients de bénéficier des dernières innovations technologiques, avantage concurrentiel non négligeable. De l'autre côté, les
                chercheurs bénéficient de retours utiles pour faire avancer leurs projets. </p>
            </div>
            <div class="col-sm-4 outer-bottom-xs text-center">
              <i class="margin-bottom-20 text-center icon icon-sun icn lg"></i>
              <h2 class="margin-bottom-20 text-center">Acteur de(s) transition(s)</h2>
              <p class="text-medium text-justify">
                Nous sommes convaincus du rôle des nouvelles technologies et du Web comme facilitateur dans le processus de changement de notre société et de nos modes de consommation.
                <br>
                Nous nous engageons donc aux côtés des acteurs de la transition énergétique, sociale et écologique notamment en les assistant dans l’organisation de leurs communautés.
              </p>
            </div>
          </div>
        </div>
      </section>


      <section id="team">
        <div class="container inner-top inner-bottom-xs">

          <div class="row">
            <div class="col-md-8 col-sm-10 center-block text-center">
              <header>
                <h1>Notre équipe :</h1>
              </header>
            </div>
          </div>

          <div class="row inner-top-sm text-center">

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
              <figure class="member">
                <div class="icon-overlay icn-link">
                  <img src="assets/images/team/ndelaforge.png" class="img-circle">
                </div>
                <figcaption>
                  <h2>
                    Nicolas Delaforge
                    <span>Co-fondateur, CEO & CTO</span>
                  </h2>
                </figcaption>
              </figure>
            </div>

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
              <figure class="member">

                <div class="icon-overlay icn-link">
                  <div class="icon-overlay icn-link">
                    <img src="assets/images/team/mleitzelman.png" class="img-circle">
                  </div>
                  <figcaption>
                    <h2>
                      Mylène Leitzelman
                      <span>Co-fondatrice et Data scientist</span>
                    </h2>
                  </figcaption>
              </figure>
            </div>

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
              <figure class="member">
                <div class="icon-overlay icn-link">
                  <img src="assets/images/team/mrogelja.png" class="img-circle">
                </div> 
                <figcaption>
                  <h2>
                    Mathieu Rogelja
                    <span>Responsable Front-end</span>
                  </h2>
                </figcaption>
              </figure>
            </div>

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
              <figure class="member">
                <div class="icon-overlay icn-link">
                  <img src="assets/images/team/prlherisson.png" class="img-circle">
                </div>
                <figcaption>
                  <h2>
                    Pierre-René Lherisson
                    <span>Développeur Back-end</span>
                  </h2>
                </figcaption>
              </figure>
            </div>

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
              <figure class="member">
                <div class="icon-overlay icn-link">
                  <img src="assets/images/team/aibrahim.jpg" class="img-circle">
                </div> 
                <figcaption>
                  <h2>
                    Ibrahim Alain
                    <span>Développeur Front-end</span>
                  </h2>
                </figcaption>
              </figure>
            </div>

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
              <figure class="member">
                <div class="icon-overlay icn-link">
                  <img src="assets/images/team/flimpens.jpg" class="img-circle">
                </div> 
                <figcaption>
                  <h2>
                    Freddy Limpens
                    <span>Chef de projet</span>
                  </h2>
                </figcaption>
              </figure>
            </div>
          </div>

          <div class="row">
            <div class="col-md-8 col-sm-10 center-block text-center">
              <header>
                <h1>Nos partenaires :</h1>
              </header>
            </div>
          </div>

          <div class="row inner-top-sm text-center">
            <div class="vertical-center col-sm-6 inner-right-xs inner-bottom-xs">
              <a href="https://www.devops.works/" target="_blank">
                <img src="assets/images/art/devops.jpg" class="img-350">
              </a>
            </div>
            <div class="vertical-center col-sm-6 inner-left-xs inner-bottom-xs">
              <a href="https://www.elzeard.co/" target="_blank">
                <img src="assets/images/art/elzeard.jpg" class="img-350"></a>
            </div>
          </div>
          <div class="row text-center">
            <div class="vertical-center col-sm-6 inner-right-xs inner-bottom-xs">
              <a href="https://www.ontotext.com/" target="_blank">
                <img src="assets/images/art/ontotext.jpg" class="img-350"></a>
            </div>
            <div class="vertical-center col-sm-6 inner-left-xs inner-bottom-xs">
              <a href="https://team.inria.fr/wimmics/" target="_blank">
                <img src="assets/images/art/winmics.jpg" class="img-350"></a>
            </div>
          </div>
        </div>
      </section>

      <section id="testimonials" class="light-bg img-bg-softer" style="background-image: url(assets/images/art/pattern-background01.jpg);">
        <div class="container inner">

          <div class="row">
            <div class="col-md-6 col-sm-8 center-block text-center">
              <header>
                <h1>Ils parlent de nous</h1>
              </header>
            </div>
          </div>

          <div class="row">
            <div class="col-md-7 col-sm-9 center-block text-center">
              <div id="owl-testimonials" class="owl-carousel owl-outer-nav owl-ui-md">

                <div class="item">
                  <blockquote>
                    <p>
                      Nous avons aussi été frappés par la variété d’entreprises
                      coopératives issues
                      tant du secteur industriel que de startups comme Mnémotix -
                      née d’un projet
                      de recherche de Sophia Antipolis -, ou 1DLab qui fait du
                      streaming musical
                      alternatif, incubée au Numa. Les formes coopératives sont
                      intrinsèquement
                      prédisposées à se déployer dans le numérique qui est par
                      essence un terrain
                      de création collective et d’innovation ouverte
                    </p>
                    <footer>
                      <cite>
                        Benoît Thieulin, président du Conseil National du Numérique,
                        <a href="http://www.cnnumerique.fr/wp-content/uploads/2015/12/Rapport-travail-version-finale-janv2016.pdf" target="_blank">
                          discours du 6 janvier 2015
                        </a>
                      </cite>
                    </footer>
                  </blockquote>
                </div>

                <div class="item">
                  <blockquote>
                    <p>
                      Le mouvement coopératif et les écosystèmes numériques
                      gagneraient à se
                      rapprocher davantage . Cette convergence constituerait une
                      alternative
                      positive aux craintes actuelles de captation de la valeur
                      économique et
                      sociale par de grands acteurs.
                      Les modèles d’organisation en SCOP ou en SCIC sont
                      particulièrement adaptés
                      à des projets de plateformes numériques soutenables et
                      équitables.
                      Le mode de gouvernance proposée (sociétariat, intégration de
                      parties
                      prenantes publiques et privées) intègre ainsi
                      l’innovation sociale au coeur de l’organisation de travail
                      et n’en fait plus seulement une finalité.
                    </p>
                    <footer>
                      <cite>
                        Conseil National du Numérique - Dossier de presse -
                        <a target="_blank" href="http://www.cnnumerique.fr/wp-content/uploads/2015/11/Dossier-de-presse-rapport-travail-emploi-CNNum-VF.pdf">
                          Rapport “Travail, Emploi, Numérique, les nouvelles
                          trajectoires”
                        </a>
                        (janvier 2016)
                      </cite>
                    </footer>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>

      <section id="clients">
        <div class="container inner">

          <div class="row">
            <div class="col-md-6 col-sm-8 center-block text-center">
              <header>
                <h1>Ils nous ont fait confiance</h1>
              </header>
            </div>
          </div>

          <div class="row thumbs gap-lg inner-top-sm">

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>ADEME</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/ademe.png" alt="ADEME">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>The Green Communication</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/tgc.png" alt="The Green Communication">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>Fondation des Galeries Lafayette</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/fegl.png" alt="Fondation d'Entreprise Galeries Lafayette">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>CNRS</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/cnrs.png" alt="Centre National de la Recherche Scientifique">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>INRIA</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/inria.png" alt="Institut National de Recherche en Informatique et Automatique">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>SICTIAM</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/sictiam.png" alt="SICTIAM">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>Infotel</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/infotel.png" alt="Infotel">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>Groupe PSA</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/psa.png" alt="Groupe PSA">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>MindMatcher</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/mindmatcher.png" alt="MindMatcher">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>1D Lab</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/1dlab.png" alt="1D Lab">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>ISTHIA</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/isthia.png" alt="ISTHIA">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>Villa Arson Nice</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/va.png" alt="Villa Arson Nice">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>Université Côte d'Azur</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/uca.png" alt="Université Côte d'Azur">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>Université Toulouse Jean Jaurès</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/ujjt.png" alt="Université Toulouse Jean Jaurès">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>Ministère de la culture et la communication</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/ministereculture.png" alt="Ministère de la culture et la communication">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>Educlever</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/educlever.png" alt="Educlever">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>CoreKap</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/corekap.png" alt="CoreKap">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>Clairsienne</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/clairsienne.png" alt="Clairsienne">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>UTC</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/utc.jpeg" alt="UTC">
              </figure>
            </div>

            <div class="col-sm-3 col-xs-4 thumb">
              <figure>
                <figcaption class="text-overlay">
                  <div class="info">
                    <h4>2IF</h4>
                  </div>
                </figcaption>
                <img src="assets/images/clients/2if.jpeg" alt="2IF">
              </figure>
            </div>
          </div>
        </div>
      </section>

      <section id="actus" class="blue-background-image">
        <div class="container inner-top inner-bottom-sm">
          <div class="row">
            <div class="col-md-8 col-sm-9 center-block text-center ">
              <h1 class="text-white">
                Suivez-nous
              </h1>
              <p>Notre actualité est sur les réseaux sociaux</p>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div class="container inner-xs">
          <div class="row">
            <div class="col-md-8 col-sm-10 center-block text-center gap-lg">
              <a class="twitter-timeline" data-width="600" data-height="500" href="https://twitter.com/mnemotix?ref_src=twsrc%5Etfw">
                <div class="inner">Tweets by mnemotix</div>
              </a>
              <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
          </div>
        </div>
      </section>




      <section id="recrutement" class="orange-background-image">
        <div class="container inner-top inner-bottom-sm">
          <div class="row">
            <div class="col-md-8 col-sm-9 center-block text-center ">
              <h1 class="text-white">Recrutement</h1>
              <p class="text-white">Mnemotix a besoin de vos talents et recherche...</p>
            </div>
          </div>
        </div>
      </section>



      <section id="product">
        <div class="container inner">
          <!-- <div style="text-align: center">
            <div style="display: inline-block; text-align: left;">
              <ul>
                <li>
                  <h2>- Un-e développeur-se Front-End</h2>
                </li>
                <li>
                  <h2>- Un-e développeur-se Back-End</h2>
                </li>
                <li>
                  <h2>- Un-e chef-fe de projet</h2>
                </li>
              </ul> 
            </div>
          </div> --> 
          <div style="text-align: center">
            <a href="mailto:contact@mnemotix.com?subject=candidature spontannée">
              <h2 class="orange-button">
                Vous souhaitez déposer une candidature spontannée ?
              </h2>
            </a>
          </div>
        </div>
      </section>

      <section id="contact" class="blue-background-image">
        <div class="container inner">
          <div class="row">

            <div class="col-md-12 text-center">
              <header>
                <h1 class="text-white">Contactez-nous</h1>
                <p>Une équipe à votre service.</p>
              </header>
            </div>

          </div>
        </div>
      </section>

      <section id="contact-form">
        <div class="container inner">
            <div class="row">

              <div class="col-sm-6 inner-right-sm">

                <h2>Laissez-nous un message</h2>

                <form id="contactform" class="forms" action="contact.php" method="post">
                  <div class="row">
                    <div class="col-sm-8">
                      <label for="name"></label>
                      <input type="text" id="name" name="name" class="form-control" placeholder="Nom *">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-8">
                      <label for="email"></label>
                      <input type="email" id="email" name="email" class="form-control" placeholder="Email *">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-8">
                      <label for="subject"></label>
                      <input type="text" id="subject" name="subject" class="form-control" placeholder="Sujet">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <label for="message"></label>
                      <textarea id="message" name="message" class="form-control" placeholder="Message ..."></textarea>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-default btn-submit">
                    Envoyer
                  </button>
                </form>

                <div id="response"></div>
              </div>

              <div class="mnx-infos col-sm-6 inner-left-sm border-left">

                <h2>Contacts</h2>

                <ul class="contacts">
                  <li><i class="icon-mobile contact"></i> +33 6 16 22 71 18</li>
                  <li><a href="mailto:contact@mnemotix.com"><i class="icon-mail contact"></i> contact@mnemotix.com</a>
                  </li>
                  <li class="flexcontainer">
                    <i class="icon-location contact"></i>
                    <p>Hôtel d'Entreprise Cannes Pays de Lérins<br>
                      Allée Maurice Bellonte<br>
                      06210 MANDELIEU-LA-NAPOULE<br>FRANCE</p>
                  </li>
                  <li style="padding-top: 25px">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23134.651212497796!2d6.939095864143175!3d43.54755625459433!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12ce82db922fb26b%3A0xc7e4956d40e9ac2e!2s318+Mont%C3%A9e+de+la+Carraire%2C+06210+Mandelieu-la-Napoule!5e0!3m2!1sfr!2sfr!4v1444306164181" width="320" height="240" frameborder="0" style="border:0" allowfullscreen></iframe>
                  </li>
                </ul>

                <h3>Suivez nous</h3>
                <ul class="contacts">
                  <li><a href="https://twitter.com/mnemotix" target="blank"><i class="icon-twitter contact"></i> Twitter</a></li>

                </ul>


              </div>

            </div>
         </div>
      </section>
    </main>


    <!-- ============================================================= FOOTER ============================================================= -->

    <footer class="dark-bg">
      <?php include 'includes/footer.php'; ?>
    </footer>


    <!-- =============================================================== JS =============================================================== -->

    <!-- Scripts -->
    <?php include 'includes/scripts.php'; ?>

  </body>

  </html>